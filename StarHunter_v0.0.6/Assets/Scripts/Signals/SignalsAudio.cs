﻿using UnityEngine;

public class SignalsAudio
{

    public static void Dispatch_PlayAudioGameplay(AUDIO_GAMEPLAY key,int photonViewID_audiosource, bool online)//#Implemented
    {
        
        if (Listen_PlayAudioGameplay == null || Listen_PlayAudioGameplay.GetInvocationList().Length < 1) return;
        Listen_PlayAudioGameplay(key, photonViewID_audiosource, online);
    }
    public delegate void OnPlayAudioGameplay(AUDIO_GAMEPLAY key, int photonViewID_audiosource, bool online);
    public static OnPlayAudioGameplay Listen_PlayAudioGameplay;



    public static void Dispatch_InstanceAudioGameplay(AUDIO_GAMEPLAY key, Vector2 position, bool autoDestroy,bool online)//#Implemented
    {
        if (Listen_InstanceAudioGameplay == null || Listen_InstanceAudioGameplay.GetInvocationList().Length < 1) return;
        Listen_InstanceAudioGameplay(key, position, autoDestroy,online);
    }
    public delegate void OnInstanceAudioGameplay(AUDIO_GAMEPLAY key, Vector2 position, bool autoDestroy,bool online);
    public static OnInstanceAudioGameplay Listen_InstanceAudioGameplay;


    public static void Dispatch_InstanceAudioUI(AUDIO_UI key, bool loop, float timeDestroy)//#Implemented
    {
        if (Listen_InstanceAudioUI == null || Listen_InstanceAudioUI.GetInvocationList().Length < 1) return;
        Listen_InstanceAudioUI(key, loop, timeDestroy);
    }
    public delegate void OnInstanceAudioUI(AUDIO_UI key, bool loop, float timeDestroy);
    public static OnInstanceAudioUI Listen_InstanceAudioUI;



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class SignalsGame
{



    public static void Dispatch_ChangeZoomCamera(float zoom)//#Implemented
    {
        if (Listen_ChangeZoomCamera == null || Listen_ChangeZoomCamera.GetInvocationList().Length < 1) return;
        Listen_ChangeZoomCamera(zoom);
    }

    public delegate void OnChangeZoomCamera(float zoom);
    public static OnChangeZoomCamera Listen_ChangeZoomCamera;



    public static void Dispatch_OnFire(Weapon weapon)//#Implemented
    {
        if (Listen_OnFire == null || Listen_OnFire.GetInvocationList().Length < 1) return;
        Listen_OnFire(weapon);
    }
    public delegate void OnFire(Weapon weapon);
    public static OnFire Listen_OnFire;
    


    public static void Dispatch_OnReloadedWeapon(Weapon weapon)//#Implemented
    {
        if (Listen_OnReloadedWeapon == null || Listen_OnReloadedWeapon.GetInvocationList().Length < 1) return;
        Listen_OnReloadedWeapon(weapon);
    }
    public delegate void OnReloadedWeapon(Weapon weapon);
    public static OnReloadedWeapon Listen_OnReloadedWeapon;


    public static void Dispatch_OnWeaponEquip(Weapon weapon)//#Implemented
    {
        if (Listen_OnWeaponEquip == null || Listen_OnWeaponEquip.GetInvocationList().Length < 1) return;
        Listen_OnWeaponEquip(weapon);
    }
    public delegate void OnWeaponEquip(Weapon weapon);
    public static OnWeaponEquip Listen_OnWeaponEquip;








    public static void Dispatch_OnGetScore(int ViewId,int scorePluss)//#Implemented
    {
        if (Listen_OnGetScore == null || Listen_OnGetScore.GetInvocationList().Length < 1) return;
        Listen_OnGetScore(ViewId,scorePluss);
    }
    public delegate void OnGetScore(int viewId,int scorePluss);
    public static OnGetScore Listen_OnGetScore;

    public static void Dispatch_OnGetWeapon(int ViewId, WEAPONTYPE weapon)//#Implemented
    {
        if (Listen_OnGetWeapon == null || Listen_OnGetWeapon.GetInvocationList().Length < 1) return;
        Listen_OnGetWeapon(ViewId, weapon);
    }
    public delegate void OnGetWeapon(int viewId, WEAPONTYPE weapon);
    public static OnGetWeapon Listen_OnGetWeapon;

    public static void Dispatch_OnGetLife(int ViewId, int value)//#Implemented
    {
        if (Listen_OnGetLife == null || Listen_OnGetLife.GetInvocationList().Length < 1) return;
        Listen_OnGetLife(ViewId, value);
    }
    public delegate void OnGetLife(int viewId, int value);
    public static OnGetLife Listen_OnGetLife;


    public static void Dispatch_OnKill(int ViewId_assassin, int ViewId_Vitime)//#Implemented
    {
        if (Listen_OnKill == null || Listen_OnKill.GetInvocationList().Length < 1) return;
        Listen_OnKill(ViewId_assassin, ViewId_Vitime);
    }
    public delegate void OnKill(int ViewId_assassin, int ViewId_Vitime);
    public static OnKill Listen_OnKill;


    public static void Dispatch_OnDie(int ViewId_assassin, int ViewId_Vitime)//#Implemented
    {
        if (Listen_OnDie == null || Listen_OnDie.GetInvocationList().Length < 1) return;
        Listen_OnDie(ViewId_assassin, ViewId_Vitime);
    }
    public delegate void OnDie(int ViewId_assassin, int ViewId_Vitime);
    public static OnDie Listen_OnDie;

    public static void Dispatch_OnRestart()//#Implemented
    {
        if (Listen_OnRestart == null || Listen_OnRestart.GetInvocationList().Length < 1) return;
        Listen_OnRestart();
    }
    public delegate void OnRestart();
    public static OnRestart Listen_OnRestart;

    public static void Dispatch_OnMyPlayerSpawn(int photonViewID)//#Implemented
    {
        if (Listen_OnMyPlayerSpawn == null || Listen_OnMyPlayerSpawn.GetInvocationList().Length < 1) return;
        Listen_OnMyPlayerSpawn(photonViewID);
    }
    public delegate void OnOnMyPlayerSpawn(int photonViewID);
    public static OnOnMyPlayerSpawn Listen_OnMyPlayerSpawn;

    
    public static void Dispatch_RemoveBox(GameObject box)//#Implemented
    {
        if (Listen_OnRemoveBox == null || Listen_OnRemoveBox.GetInvocationList().Length < 1) return;
        Listen_OnRemoveBox(box);
    }
    public delegate void OnRemoveBox(GameObject box);
    public static OnRemoveBox Listen_OnRemoveBox;



}

﻿using UnityEngine;
public class SignalsGfx
{
    public static void Dispatch_InstanceGFX(GFXKEY key, Vector2 ps)//#Implemented
    {
        if (Listen_InstanceGFX == null || Listen_InstanceGFX.GetInvocationList().Length < 1) return;
        Listen_InstanceGFX(key, ps);
    }
    public delegate void OnInstanceGFX(GFXKEY key, Vector2 ps);
    public static OnInstanceGFX Listen_InstanceGFX;

}

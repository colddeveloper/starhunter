﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
    
public class ShowNickName : MonoBehaviour
{

    [SerializeField] PhotonView _pv;
    [SerializeField] TextMeshPro _tmp;
    [SerializeField] Transform _nickNameContent;
    
    private void Start()
    {
        if (!_pv.IsMine) return;
        if (tag == "BOT")
            _pv.RPC("ChangeTextBot", RpcTarget.AllBuffered, "BOT " + _pv.ViewID);
        else
            UpdateTextPlayer();

        SignalsGame.Listen_OnGetScore += OnGetScore;
    }

    private void OnDestroy()
    {
        SignalsGame.Listen_OnGetScore -= OnGetScore;
    }

    void OnGetScore(int id, int amount)
    {
        UpdateTextPlayer();
    }

    void UpdateTextPlayer()
    {
        int score = PlayerInfo.Score;
        _pv.RPC("ChangeTextPlayer", RpcTarget.OthersBuffered, string.Concat(PlayerInfo.Name, "<color=#00D1FF>\n", "Stars:", score.ToString(), "</color>"));
    }

    [PunRPC]
    void ChangeTextPlayer(string txt)
    {
        if(_pv.CompareTag("Player"))
        _tmp.text = txt;
    }
    [PunRPC]
    void ChangeTextBot(string txt)
    {
        if (_pv.CompareTag("BOT"))
            _tmp.text = txt;
    }



    private void Update()
    {
        _nickNameContent.transform.rotation = Quaternion.identity;
    }

}

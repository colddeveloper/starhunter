﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using TMPro;
public class ConnectToPhoton : MonoBehaviourPunCallbacks{

    [SerializeField] TextMeshProUGUI debugNet;
    [SerializeField] GameObject[] activeOnConnectToServer;

    private void Awake()
    {
        //debugNet.text = "Connecting to the server";
        Window.OpenConnectingUI();
        PhotonNetwork.GameVersion = "0.0.1";
        PhotonNetwork.ConnectUsingSettings();
        StartCoroutine("WaitConnection");
    }
    
    IEnumerator WaitConnection()
    {
        yield return new WaitForSeconds(20);
        DefaultWindowMessages.ErrorConnection();
    }
   

    public override void OnConnectedToMaster()
    {
        StopCoroutine("WaitConnection");
        Window.CloseConnectingUI();
        base.OnConnectedToMaster();        
        PhotonNetwork.SendRate = 60;
        //debugNet.text = "Connected to the server";
        foreach (GameObject obj in activeOnConnectToServer) obj.SetActive(true);
    }

  




    




}

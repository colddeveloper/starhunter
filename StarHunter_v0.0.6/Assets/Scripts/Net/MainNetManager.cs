﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class MainNetManager : MonoBehaviourPunCallbacks{

    [SerializeField] protected GameConfig _config;

    private void Start()
    {         
         PhotonNetwork.JoinLobby();
         StartCoroutine("WaitLobbyConnection");
    }

    IEnumerator WaitLobbyConnection()
    {
        yield return new WaitForSeconds(3);
        Window.OpenConnectingUI();
        yield return new WaitForSeconds(7);
        if (_isConnectedToLobby == false)
        DefaultWindowMessages.ErrorConnection();
        print("Disconnect fail lobby");
    }

    bool _isConnectedToLobby;
    public override void OnJoinedLobby()
    {
        _isConnectedToLobby = true;
        StopCoroutine("WaitLobbyConnection");
        Window.CloseConnectingUI();
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        //Create a new room for every x players connected in a room
        //Crie uma sala nova a cada x players conectados em uma sala

        int numRooms = PhotonNetwork.CountOfRooms;
        if (numRooms == 0)
        {
            CreateRoom(numRooms.ToString());
            return;
        }
        if (numRooms >= 0)
        {
            if(roomList[numRooms-1].PlayerCount >= _config.MaxPlayersByRoom)
            {
                CreateRoom((numRooms).ToString());
                return;
            }            
            PhotonNetwork.JoinRoom((numRooms-1).ToString());
        }

    }


    void CreateRoom(string nameRoom)
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = _config.MaxPlayersByRoom;
        PhotonNetwork.CreateRoom(nameRoom, roomOptions, TypedLobby.Default);
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        print("Fail create room");
        base.OnCreateRoomFailed(returnCode, message);        
        StartCoroutine(WaitToShowDefaultError());
    }
    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        print("Fail join room");        
        StartCoroutine(WaitToShowDefaultError());
    }
    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        print("Disconnected from server");
        StartCoroutine(WaitToShowDefaultError());
        
    }
    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        base.OnMasterClientSwitched(newMasterClient);
        print("Master has be disconnected");
        StartCoroutine(WaitToShowDisconnectedFromMaster());

    }

    IEnumerator WaitToShowDefaultError()
    {
        yield return new WaitForSecondsRealtime(1);
        DefaultWindowMessages.ErrorConnection();
    }
    IEnumerator WaitToShowDisconnectedFromMaster()
    {
        yield return new WaitForSecondsRealtime(1);
        DefaultWindowMessages.MessageDisconnectedFromMasterClient();
    }

}

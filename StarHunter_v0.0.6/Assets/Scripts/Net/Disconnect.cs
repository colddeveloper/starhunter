﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class Disconnect : MonoBehaviour
{

    private void Start()
    {        
        if (PhotonNetwork.IsConnected) PhotonNetwork.Disconnect();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MainChoiceNick : MonoBehaviour
{
    [SerializeField] GameObject[] m_activeOnChoiceName;

    private void Start()
    {
        WindowManager WM = Window.InputText("Nickname", "Enter nickname");
        WM.ButtonOk.onClick.AddListener(delegate { OnNicknameChoiced(WM.InputText.text);});
    }

    private void OnNicknameChoiced(string nickname)
    {
        if(string.IsNullOrEmpty(nickname) || string.IsNullOrWhiteSpace(nickname))
        {
            //#Nick name not accept
            WindowManager WM = Window.Error("Error", "Please choose a valid nickname!");
            WM.ButtonOk.onClick.AddListener(Start);
            return;
        }
        PlayerInfo.SetNickName(nickname);
        foreach (GameObject obj in m_activeOnChoiceName) obj.SetActive(true);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ActiveinTime : MonoBehaviour
{
    [SerializeField] GameObject[] objts;
    [SerializeField] int delay;
    [SerializeField] bool onAwake;
    [SerializeField] bool active = true;
    private void Awake()
    {
        if (onAwake) ActionActive();
    }
    public void ActionActive()
    {
        StartCoroutine(Active());
    }
    private IEnumerator Active()
    {
        yield return new WaitForSeconds(delay);
        foreach (GameObject obj in objts) obj.SetActive(active);
    }
}

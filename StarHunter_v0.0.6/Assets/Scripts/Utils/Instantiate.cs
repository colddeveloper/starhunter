﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiate : MonoBehaviour
{
    [SerializeField] GameObject _obj;
    [SerializeField] Vector2 _ps;
    [SerializeField] bool _onAwake;

    private void Awake()
    {
        if (_onAwake)
            ActionInstantiate();
    }

    public void ActionInstantiate()
    {
        Instantiate(_obj,_ps,_obj.transform.rotation);
    }
}

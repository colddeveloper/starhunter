﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetCurrentStatus : MonoBehaviour
{
    private void Awake()
    {
        PlayerInfo.StatusReset();
    }
}

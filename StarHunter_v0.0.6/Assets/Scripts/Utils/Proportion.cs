﻿
public class Calcs{

    public static float Proportion(float currentValue, float inMin , float inMax, float outMin, float outMax) {

        float newValue = (currentValue - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
        return newValue;
	}
}

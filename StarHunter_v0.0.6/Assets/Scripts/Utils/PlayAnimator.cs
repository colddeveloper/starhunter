﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimator : MonoBehaviour
{
    [SerializeField] bool onAwake;
    [SerializeField] string nameAnim;
    [SerializeField] Animator ant;
    private void Awake()
    {
        if (onAwake) ActionPlay();
    }
    public void ActionPlay()
    {
        ant.Play(nameAnim);
    }
}

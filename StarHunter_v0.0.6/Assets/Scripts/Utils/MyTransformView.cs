﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using Photon.Pun;

/*
 #Lag compensation for position and rotation
 */ 

public class MyTransformView : MonoBehaviourPun, IPunObservable
{
    [SerializeField] bool _enableTeleport;
    Rigidbody2D m_rigidbody;
    PhotonView m_view;
    Vector2 networkPosition;
    float networkRotation;
    

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_view = GetComponent<PhotonView>();
    }

    [System.Obsolete]
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(this.m_rigidbody.position);
            stream.SendNext(this.m_rigidbody.rotation);
            stream.SendNext(this.m_rigidbody.velocity);            
        }
        else
        {
            networkPosition = (Vector2)stream.ReceiveNext();
            networkRotation = (float)stream.ReceiveNext();
            m_rigidbody.velocity = (Vector2)stream.ReceiveNext();
            float lag = Mathf.Abs((float)(PhotonNetwork.Time - info.timestamp));
            networkPosition += (this.m_rigidbody.velocity * lag);
        }
    }

    public void FixedUpdate()
    {
        if (!photonView.IsMine)        
            if (_enableTeleport && Vector2.Distance(m_rigidbody.position, networkPosition) >= 1.5f)
                InstaPosition();//#Teleport by distance
            else
                LagCompensationLerp();
    }
    
    void LagCompensationLerp()
    {
        m_rigidbody.position = Vector3.MoveTowards(m_rigidbody.position, networkPosition, Time.fixedDeltaTime);
        m_rigidbody.SetRotation(networkRotation);
    }
    void InstaPosition()
    {
        m_rigidbody.position = networkPosition;
    }
}

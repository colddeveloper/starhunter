﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Blink : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _tmp;
    [SerializeField] Color _colorBlink;
    [SerializeField] float _speed;
    [SerializeField] float _frequency = 0.2f;
    Color _defaultColor;
    private void Awake()
    {
        _defaultColor = _tmp.color;
    }
    void Update()
    {
        if (!enabled) return;
        Color mcolor = Color.Lerp(_defaultColor, _colorBlink, Mathf.PingPong(Time.time, _frequency) * _speed);
        _tmp.color = mcolor;        
    }
}

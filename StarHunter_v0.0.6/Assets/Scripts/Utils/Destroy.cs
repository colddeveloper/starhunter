﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    [SerializeField] bool  onAwake;
    [SerializeField] bool unscaledTime;
    [SerializeField] float delay;
    [SerializeField] GameObject obj;    

    public float Delay { set => delay = value; }

    private void Awake()
    {
        if (onAwake) ActionDestroy();
    }
    public void ActionDestroy()
    {
        StartCoroutine(DestroyInTime(delay));
    }
    private IEnumerator DestroyInTime(float seconds)
    {
        if(!unscaledTime)
            yield return new WaitForSeconds(seconds);
        else
            yield return new WaitForSecondsRealtime(seconds);
        Destroy(obj.gameObject);
    }

}

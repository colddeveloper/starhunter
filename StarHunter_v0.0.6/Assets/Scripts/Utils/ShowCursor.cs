﻿using UnityEngine;
public class ShowCursor : MonoBehaviour
{
    [SerializeField] bool _enable;
    void Start()
    {
        Cursor.visible = _enable;
    }
}

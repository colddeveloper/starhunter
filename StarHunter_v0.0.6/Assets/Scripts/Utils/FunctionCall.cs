﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FunctionCall : MonoBehaviour
{




    public void StartInvokeCall()
    {
        foreach (InvokeCall scr in m_invokes)
            scr.Action();
    }
    public void StartSendMessagesCall()
    {
        foreach (SendMessageCall scr in m_sendMessages)
            scr.Action();
    }

    [SerializeField] private InvokeCall[] m_invokes;
    [Serializable]
    public class InvokeCall{
        
        [SerializeField] private string m_functionName;
        [SerializeField] private MonoBehaviour m_scrToCall;
        [SerializeField] private float m_delay;

        public void Action()
        {
            m_scrToCall.Invoke(m_functionName, m_delay);
        }        
    }
    

    [SerializeField] private SendMessageCall[] m_sendMessages;
    [Serializable]
    public class SendMessageCall
    {

        [SerializeField] private string m_functionName;
        [SerializeField] private GameObject m_objToCall;
        [SerializeField] private UnityEngine.Object m_object;

        public void Action()
        {
            m_objToCall.SendMessage(m_functionName, m_object);
        }
    }
}

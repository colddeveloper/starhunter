﻿using UnityEngine;
public class SaveData
{
    static int _record;
    public static int Record { get => _record; }

    public static int LoadRecord()
    {
        _record = PlayerPrefs.GetInt("record");
        return _record;
    }
    public static void SaveRecord(int value)
    {
        _record = value;
        PlayerPrefs.SetInt("record", _record);
    }
}

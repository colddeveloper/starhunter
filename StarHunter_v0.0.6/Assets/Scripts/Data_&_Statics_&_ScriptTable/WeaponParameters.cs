﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[CreateAssetMenu(fileName = "New WeaponData",menuName = "WeaponData")]
[Serializable]
public class WeaponParameters:ScriptableObject
{
    [Header("Attributes")]
    [SerializeField] float _firerate;
    [SerializeField] float _bulletSpeed;
    [SerializeField] int _damage;
    [SerializeField] int _maxBullets;
    [SerializeField] float _range;
    [SerializeField] float _vision;//Is Camera zoom
    [SerializeField] WEAPONTYPE _weaponType;
    [Header("Graphics")]
    [SerializeField] GameObject _bulletPf;
    [SerializeField] Sprite _icon;
    [Header("Audio")]
    [SerializeField] AUDIO_GAMEPLAY _audioFire;
    [SerializeField] GFXKEY _gfxFire;


    public float FireRate { get => _firerate; }
    public float BulletSpeed { get => _bulletSpeed; }    
    public float Range { get => _range; }
    public float Vision { get => _vision; }
    public int Damage { get => _damage; }
    public int MaxBullets { get => _maxBullets;}
    public WEAPONTYPE WeaponType { get => _weaponType; }
    public GameObject BulletPf { get => _bulletPf; }
    public Sprite Icon { get => _icon; }

    public AUDIO_GAMEPLAY AudioFire { get => _audioFire; }
    public GFXKEY GfxFire { get => _gfxFire; }




}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultWindowMessages : MonoBehaviour
{
    public static void ErrorConnection()
    {
        Window.CloseConnectingUI();
        WindowManager wm = Window.Error("Error Connection!", "Error connecting, try again later.");
        wm.ButtonOk.onClick.AddListener(BackMenu);
    }

    public static void MessageDisconnectedFromMasterClient()
    {        
        Window.CloseConnectingUI();
        WindowManager wm = Window.Error("Error Connection!", "Disconnected from the master server client");
        wm.ButtonOk.onClick.AddListener(BackMenu);
    }

    static void BackMenu()
    {
        PreLoad.AppearPreLoad("Menu");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class PlayerInfo
{
    //----------------------------------------
    static string _name;    
    static int _score;
    static int _kills;
    static int _dies;
    static int _viewID;
    //----------------------------------------
    public static string Name { get => _name;}
    public static int Score { get => _score; }
    public static int Kills { get => _kills; }
    public static int Dies { get => _dies; }
    public static int ViewID { get => _viewID; set => _viewID = value; }

    //----------------------------------------
    static ExitGames.Client.Photon.Hashtable _proprieties = new ExitGames.Client.Photon.Hashtable();
   
    private static void SetScore(int value)
    {
        _score = value;
        _proprieties["score"] = value;
        PhotonNetwork.LocalPlayer.CustomProperties = _proprieties;        
    }
    private static void SetKills(int value)
    {
        _kills = value;        
        var proprieties = PhotonNetwork.LocalPlayer.CustomProperties;
        _proprieties["kills"] = value;
        PhotonNetwork.LocalPlayer.CustomProperties = _proprieties;
    }
    private static void SetDies(int value)
    {
        _dies = value;        
        var proprieties = PhotonNetwork.LocalPlayer.CustomProperties;
        _proprieties["dies"] = value;
        PhotonNetwork.LocalPlayer.CustomProperties = _proprieties;
    }

    //#public functions
    public static void SetNickName(string value)
    {
        _name = value;
        PhotonNetwork.LocalPlayer.NickName = value;
    }
    public static void IncremmentScore(int value)
    {
        _score += value;
        SetScore(_score);
    }
    public static void IncremmentKills()
    {
        _kills++;
        SetKills(_kills);
    }
    public static void IncremmentDies()
    {
        _dies++;
        SetDies(_dies);
    }

    public static void ScoreReset()
    {
        SetScore(0);
    }
    public static void StatusReset()
    {
        SetScore(0);
        SetDies(0);
        SetKills(0);
    }
    //----------------------------------------




}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "GfxData", menuName = "Global/GfxData")]
public class GfxData : ScriptableObject
{
    

    [SerializeField] GFX[] _gfxStorage;



    public GameObject InstanceGfxByKey(GFXKEY key,Vector2 ps)
    {
        GameObject gfxToInstance = null;

        foreach (GFX g in _gfxStorage)
        {
            gfxToInstance = g.GetGfxByKey(key);
            if( gfxToInstance != null ) break;            
        }
        if (!gfxToInstance)
        {
            Debug.LogError(key+" Not exist GFX to Add");
            return null;
        }       
        return Instantiate(gfxToInstance, ps, gfxToInstance.transform.rotation);
    }



    [Serializable]
    public class GFX
    {
        [SerializeField] string _name;
        [SerializeField] GameObject _gfx;
        [SerializeField] GFXKEY _gfxKey;


        public GameObject GetGfxByKey(GFXKEY key)
        {
            GameObject R = null;
            if (key == _gfxKey) R = _gfx;
            return R;
        }
    }

}

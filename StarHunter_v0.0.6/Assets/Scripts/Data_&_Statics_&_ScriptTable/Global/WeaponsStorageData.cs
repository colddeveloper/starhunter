﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[CreateAssetMenu(fileName = "WeaponsStorageData",menuName = "Global/WeaponsStorageData")]
public class WeaponsStorageData : ScriptableObject
{
    [SerializeField] WeaponStorage[] _weapons;
    public WeaponStorage[] Weapons { get => _weapons; }



    //---------------------------------------------------------Sub Class
    [Serializable]
    public class WeaponStorage
    {
        [SerializeField] string _name;
        [SerializeField] string _description;
        [SerializeField] Weapon _script;
        public Weapon Script { get => _script; }        

        public bool CompareType(WEAPONTYPE type)
        {
            bool R = false;
            if (_script.Data.WeaponType == type) R = true;
            return R;
        }
    }
    //---------------------------------------------------------END SubClass
    public Weapon GetWeaponByType(WEAPONTYPE type)
    {
        Weapon R = null;
        foreach (WeaponStorage ws in _weapons)
            if (ws.CompareType(type)) R = ws.Script;
        return R;
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[CreateAssetMenu(fileName = "AudioData",menuName = "Global/AudioData")]
public class AudioData : ScriptableObject
{
    [SerializeField] UI[] m_ui;
    [SerializeField] Gameplay[] m_gameplay;

    //#can get only audio
    public UI GetUI(AUDIO_UI key)
    {
        UI mui = null;
        foreach (UI ui in m_ui)
            if (ui.KeyM == key) mui = ui;
            return mui;
    }
    //can direct instance audio
    public AudioSource Instance_UI(AUDIO_UI key,bool loop,float timeDestroy)
    {
        UI mui = GetUI(key);
        AudioClip clip = mui.Clip;
        GameObject obj = new GameObject(mui.Name);
        AudioSource As = obj.AddComponent<AudioSource>();
        As.clip = clip;
        As.loop = loop;
        As.Play();
        Destroy(obj, timeDestroy);
        return As;
    }

    //#can get only audio
    public AudioClip GetGameplay(AUDIO_GAMEPLAY key)
    {
        AudioClip R = null;
        foreach (Gameplay g in m_gameplay)
            if (g.KeyM == key) R = g.Clip;
        return R;
    }

    //can direct instance audio
    public AudioSource Instance_Gameplay(AUDIO_GAMEPLAY key, Vector2 position, bool autoDestroy)
    {        
        AudioClip clip = GetGameplay(key);
        GameObject obj = new GameObject(string.Concat("SFX_",clip.name));
        obj.transform.position = position;
        AudioSource As = obj.AddComponent<AudioSource>();
        As.clip = clip;
        As.loop = false;
        myAudioSource mads = As.gameObject.AddComponent<myAudioSource>();
        mads.AutoDestroy = autoDestroy;
        As.Play();
        mads.Start();
        return As;
    }

    //#Classes


    [Serializable]
    public class UI
    {

        [SerializeField] string m_name;
        [SerializeField] AUDIO_UI m_key;
        [SerializeField] AudioClip m_audio;
        public AUDIO_UI KeyM { get => m_key; }
        public AudioClip Clip { get => m_audio; }
        public string Name { get => m_name; }
    }

    [Serializable]
    public class Gameplay
    {
        [SerializeField] private string m_name;
        [SerializeField] private AUDIO_GAMEPLAY m_key;
        [SerializeField] private AudioClip m_audio;

        public AUDIO_GAMEPLAY KeyM { get => m_key; }
        public AudioClip Clip { get => m_audio; }
    }

   

}

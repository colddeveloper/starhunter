﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Gameconfig",menuName = "Global/Gameconfig")]
public class GameConfig : ScriptableObject
{

    //#serialized
    [SerializeField] Vector2 _area;
    [SerializeField] Vector2 _rngSizeDecoratorStar;
    [SerializeField] float _maxSecondsToAddNextProp;
    [SerializeField] byte _maxPlayersByRoom = 20;
    
    //#public
    public Vector2 Area { get => _area; }
    public Vector2 RangeSizeDecoratorStar { get => _rngSizeDecoratorStar; }
    public float MaxSecondsToAddProp { get => _maxSecondsToAddNextProp; }
    public byte MaxPlayersByRoom { get => _maxPlayersByRoom; }

    //#Utils
    public Vector2 GetRandomPositionByArea()
    {
        float margin = 0.8f;
        return new Vector2(Random.Range(-_area.x * margin, _area.x * margin), Random.Range(-_area.y * margin, _area.y * margin));
    }

    public bool OutScenario(Vector2 ps)
    {        
        //Check if position out scenario limits
        float margin = 0.2f;
        return ps.x > _area.x - margin || ps.x < -_area.x + margin || ps.y > _area.y - margin || ps.y < -_area.y + margin;
    }
}

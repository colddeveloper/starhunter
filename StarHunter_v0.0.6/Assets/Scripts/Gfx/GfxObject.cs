﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class GfxObject : MonoBehaviour
{
    [SerializeField] float _destroyInTIme;
    //[SerializeField] PhotonView _pv;
    private void Awake()
    {
        //if (!_pv && GetComponent<PhotonView>()) _pv = GetComponent<PhotonView>();
    }
    private void Start()
    {        
        StartCoroutine(DestroyVfx());
    }
    private IEnumerator DestroyVfx()
    {
        yield return new WaitForSeconds(_destroyInTIme);
        //PhotonNetwork.Destroy(gameObject);
        Destroy(gameObject);
    }

}

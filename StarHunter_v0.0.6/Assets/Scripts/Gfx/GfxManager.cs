﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class GfxManager : MonoBehaviour
{
    [SerializeField] GfxData _data;
    [SerializeField] PhotonView _pv;
    private void Start()
    {
        SignalsGfx.Listen_InstanceGFX += OnInstanceGFX;
    }
    private void OnDestroy()
    {
        SignalsGfx.Listen_InstanceGFX -= OnInstanceGFX;
    }

    private void OnInstanceGFX(GFXKEY key,Vector2 ps)
    {
        if (key == GFXKEY.NONE) return;
        _pv.RPC("InstanceGFX_RPC", RpcTarget.All, key, ps);
    }

    [PunRPC]
    private void InstanceGFX_RPC(GFXKEY key, Vector2 ps)
    {
        _data.InstanceGfxByKey(key, ps);
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : MonoBehaviour
{
    public static WindowManager InputText(string tittle,string placeholder)
    {
        
        GameObject obj = Instantiate(Resources.Load<GameObject>("Window/Canvas_window_input"));
        WindowManager wm = obj.GetComponent<WindowManager>();
        wm.Tittle.text = tittle;
        wm.PlaceHolderInput.text = placeholder;
        return wm;
    }
    public static WindowManager Alert(string tittle, string description)
    {
        
        GameObject obj = Instantiate(Resources.Load<GameObject>("Window/Canvas_window_alert"));
        WindowManager wm = obj.GetComponent<WindowManager>();
        wm.Tittle.text = tittle;
        wm.Description.text = description;
        return wm;
    }

    public static WindowManager Error(string tittle, string description)
    {

        GameObject obj = Instantiate(Resources.Load<GameObject>("Window/Canvas_window_error"));
        WindowManager wm = obj.GetComponent<WindowManager>();
        wm.Tittle.text = tittle;
        wm.Description.text = description;
        return wm;
    }


    static GameObject _connecingUI;
    public static void OpenConnectingUI()
    {
        if (_connecingUI) return;
        _connecingUI = Instantiate(Resources.Load<GameObject>("Window/Canvas_connecting"));
    }
    public static void CloseConnectingUI()
    {
        if (!_connecingUI) return;
        _connecingUI.GetComponent<WindowManager>().CloseCustomAnimation("Window_close_connecting");
        _connecingUI = null;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WindowManager : MonoBehaviour
{
    
    [SerializeField] private TextMeshProUGUI m_tittle;
    [SerializeField] private Button m_buttonOk;
    [SerializeField] private Animator m_ant;
    [SerializeField] private TextMeshProUGUI m_description;
    [SerializeField] private TMP_InputField m_input;
    [SerializeField] private TextMeshProUGUI m_placeholderInputText;

    public TextMeshProUGUI Tittle { get => m_tittle; set => m_tittle = value; }
    public TextMeshProUGUI Description { get => m_description; set => m_description = value; }
    public TMP_InputField InputText { get => m_input; set => m_input = value; }
    public TextMeshProUGUI PlaceHolderInput { get => m_placeholderInputText; set => m_placeholderInputText = value; }
    public Button ButtonOk { get => m_buttonOk; }
   

    //Called in Button Event Click
    public void Close()
    {
        m_ant.Play("Window_disappear");
    }

    public void CloseCustomAnimation(string an)
    {
        m_ant.Play(an);
    }
}

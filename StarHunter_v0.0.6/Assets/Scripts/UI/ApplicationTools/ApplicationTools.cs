﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationTools : MonoBehaviour
{
    public static void FadeOut()
    {
        Instantiate(Resources.Load<GameObject>("ApplicationTools/Canvas_fade_out"));
    }
    public static void FadeIn()
    {
        Instantiate(Resources.Load<GameObject>("ApplicationTools/Canvas_fade_in"));
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowStatus : MonoBehaviour
{
    public static WindowStatusManager Status(string name,int score,int kills,int dies,bool checkRecord)
    {
        GameObject obj = Instantiate(Resources.Load<GameObject>("Window/Canvas_window_status"));
        WindowStatusManager wm = obj.GetComponent<WindowStatusManager>();
        wm.Config(name, score, kills, dies, checkRecord);        
        return wm;
    }
}

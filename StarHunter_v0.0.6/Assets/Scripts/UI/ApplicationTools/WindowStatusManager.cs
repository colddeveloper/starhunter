﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class WindowStatusManager : MonoBehaviour
{

    [SerializeField] TextMeshProUGUI _name, _score, _kills, _dies,_record,_recordTittle;
    [SerializeField] Button _restart;
    [SerializeField] Animator _ant;

    public void Config(string mname,int score,int kills,int dies,bool checkRecord)
    {
        _name.text = mname;
        _score.text = score.ToString();
        _kills.text = kills.ToString();
        _dies.text = dies.ToString();


        int mrecord = SaveData.LoadRecord();
        if (score > mrecord && checkRecord)
        {
            _recordTittle.text = "New Record ";
            _recordTittle.GetComponent<Blink>().enabled = true;
            mrecord = score;
            SaveData.SaveRecord(mrecord);
        }
        else
        { 
            _recordTittle.text = "Record: ";
            _recordTittle.GetComponent<Blink>().enabled = false;
        }


        if (mrecord > 0)            
            _record.text = SaveData.Record.ToString();

        
        
    }

    //Called in Button Event Click
    public void Close()
    {        
        WindowManager wm = Window.Alert("Info", "Your stars will be lost.");
        wm.ButtonOk.onClick.AddListener(NotifyReset);
    }

    private void NotifyReset()
    {
        SignalsGame.Dispatch_OnRestart();
        _ant.Play("Window_disappear");
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CountStarsUiManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _tmp;

    void Start()
    {
        SignalsGame.Listen_OnGetScore += OnGetStarScore;
        SignalsGame.Listen_OnMyPlayerSpawn += UpdateTextCount;
        UpdateTextCount(PlayerInfo.ViewID);
    }
    private void OnDestroy()
    {
        SignalsGame.Listen_OnGetScore -= OnGetStarScore;
        SignalsGame.Listen_OnMyPlayerSpawn -= UpdateTextCount;
    }
    private void OnGetStarScore(int id, int amount)
    {
        UpdateTextCount(id);
    }

    private void UpdateTextCount(int id)
    {
        if (PlayerInfo.ViewID != id) return;
        StartCoroutine("IEUpdateStar");
    }

    IEnumerator IEUpdateStar()
    {
        yield return new WaitForSecondsRealtime(0.3f);
        _tmp.text = string.Concat('x', PlayerInfo.Score);
    }

}

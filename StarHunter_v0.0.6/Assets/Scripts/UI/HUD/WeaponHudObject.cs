﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class WeaponHudObject : MonoBehaviour
{
    WeaponManager _ownerWeaponManager;
    [SerializeField] Image _icon;
    [SerializeField] Image _background;
    [SerializeField] Image _edge;
    [SerializeField] TextMeshProUGUI _tmpAmount;
    [SerializeField] TextMeshProUGUI _tmpIndexKey;
    [SerializeField] WEAPONTYPE _weaponType;
    int _index;
    float _alpha = 1.0f;

    private void Awake()
    {        
        SignalsGame.Listen_OnFire += OnFire;
        SignalsGame.Listen_OnReloadedWeapon += OnReloaded;
        SignalsGame.Listen_OnWeaponEquip += OnWeaponEquip;
    }   

    private void OnDestroy()
    {
        SignalsGame.Listen_OnFire -= OnFire;        
        SignalsGame.Listen_OnReloadedWeapon -= OnReloaded;
        SignalsGame.Listen_OnWeaponEquip -= OnWeaponEquip;
    }


    private void OnFire(Weapon weapon)
    {
        if (weapon.MyWeaponManager != _ownerWeaponManager) return;
        if (weapon.Data.WeaponType != _weaponType) return;
        Color color = Color.red;
        color.a = _alpha;
        _background.color = color;
        _icon.color = color;
    }

    
    private void OnReloaded(Weapon weapon)
    {
        if (weapon.MyWeaponManager != _ownerWeaponManager) return;
        if (weapon.Data.WeaponType == _weaponType)
            LoadedColor();        
    }

    public void OnWeaponEquip(Weapon weapon)
    {
        if (weapon.MyWeaponManager != _ownerWeaponManager) return;
        UnloadColor();
        if (weapon.Data.WeaponType == _weaponType)
        {
            ChangeAlpha(1.0f);            
        }
        else
        {
            ChangeAlpha(0.05f);            
        }
    }



    public void Config(Sprite spriteIcon, WEAPONTYPE wtype,WeaponManager owner,int indexkey)
    {
        _weaponType = wtype;
        _icon.sprite = spriteIcon;
        _ownerWeaponManager = owner;
        _index = indexkey;        
    }
    public void UpdateAmount(int amount)
    {
        _tmpAmount.text = amount.ToString();
        _tmpIndexKey.text = (_index+1).ToString();
    }

    //#Utils
    void ChangeAlpha(float alpha)
    {
        _alpha = alpha;
        Color color = _icon.color;
        color.a = _alpha;        
        _background.color = color;
        _edge.color = color;
        color.a = 1;
        _icon.color = color;
    }

    private void UnloadColor()
    {
        Color color = Color.red;
        color.a = _alpha;
        _background.color = color;        
        _edge.color = color;
        color.a = 1;
        _icon.color = color;
    }
    private void LoadedColor() { 

        Color color = Color.white;
        color.a = _alpha;
        _background.color = color;        
        _edge.color = color;
        color.a = 1;
        _icon.color = color;
    }
}

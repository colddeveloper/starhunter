﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiBarr : MonoBehaviour
{
    [Header("UI BARR VARIABLES")]
    [SerializeField] Transform _objToScale;    
    [SerializeField] protected float _maxScale;//Scale value for objtToScale
    [SerializeField] protected float _minScale;//Scale value for objtToScale
    protected float _maxValue;//Life point value , Xp point value or other
    protected float _minValue;//Life point value , Xp point value or other
    protected float _currentValue;//current point Value


    public float MaxValue { get => _maxValue; set => _maxValue = value; }
    public float MinValue { get => _minValue; set => _minValue = value; }


    private void FixedUpdate()
    {
        if (!_objToScale) return;
        float scale = Calcs.Proportion(_currentValue, _minValue, _maxValue, _minScale, _maxScale);
        if (scale <= 0) scale = 0;
        _objToScale.transform.localScale = new Vector2(scale,_objToScale.transform.localScale.y);
    }

    public void SetCurrentValue(float value)
    {
        _currentValue = value;
    }


}

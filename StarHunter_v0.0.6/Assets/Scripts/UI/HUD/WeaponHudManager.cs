﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class WeaponHudManager : MonoBehaviour
{
    [SerializeField] RectTransform _painelContent;    
    [SerializeField] GameObject _weaponHudObject_pf;
    List<WeaponHudObject> _weaponosHudObjects_list;
    WeaponManager _weaponManager;
    int numWeaponsRegister;

    private void Start()
    {
        SignalsGame.Listen_OnDie += DestroyHUD;
    }
    private void OnDestroy()
    {
        SignalsGame.Listen_OnDie -= DestroyHUD;
    }
    void DestroyHUD(int assassinID,int vitimeID)
    {
        if (vitimeID == _weaponManager.Pv.ViewID)
        Destroy(gameObject);
    }
    

    public void Config(WeaponManager weaponManager)
    {
        _weaponosHudObjects_list = new List<WeaponHudObject>();        
        _weaponManager = weaponManager;
        AddWeaponHUDObject();
    }
    private void AddWeaponHUDObject()
    {
        if (_weaponManager.Weapons == null) return;
        int i = 0;
        foreach (Weapon w in _weaponManager.Weapons)
        {
            GameObject obj = Instantiate(_weaponHudObject_pf,_painelContent);
            WeaponHudObject who = obj.GetComponent<WeaponHudObject>();
            who.Config(w.Data.Icon, w.Data.WeaponType, _weaponManager,i);
            _weaponosHudObjects_list.Add(who);
            i++;
        }        
    }
    public void OnDecremmentMunition()
    {        
        UpdateAmount();        
    }

    public void UpdateAmount()
    {
        int i = 0;        
        foreach (WeaponHudObject who in _weaponosHudObjects_list)
        {
            int amount = _weaponManager.Weapons[i].Amount;                            
            who.UpdateAmount(amount);            
            if (amount <= 0) UpdateHUD();                
            i++;
        }
    }

    public void UpdateHUD()
    {
        //Clear list and update weapons hud   
        if (_weaponosHudObjects_list.Count != _weaponManager.Weapons.Count)
        {
            foreach (WeaponHudObject weaponhud_object in _weaponosHudObjects_list)
                Destroy(weaponhud_object.gameObject);
            _weaponosHudObjects_list.Clear();
            AddWeaponHUDObject();
        }        
    }
}

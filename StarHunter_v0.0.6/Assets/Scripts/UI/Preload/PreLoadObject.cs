﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PreLoadObject : MonoBehaviour
{
    [SerializeField] private Animator m_ant;
    [SerializeField] AudioData m_audiodata;
    [SerializeField] AUDIO_UI m_audioKey_appear;
    [SerializeField] AUDIO_UI m_audioKey_disappear;
    [SerializeField] AudioSource m_As;
    private string m_sceneToLoad;   

    private void Awake()
    {
        PreLoad.InPreload = true;
    }

    public void Config(string scene)
    {
        m_sceneToLoad = scene;
        AnimAppear();        

    }

    public void Load()
    {
        StartCoroutine(Load_IE());
    }

    IEnumerator Load_IE()
    {
        yield return SceneManager.LoadSceneAsync(m_sceneToLoad);
        AnimDisappear();
    }
    public void AnimAppear()
    {
        m_As.clip = m_audiodata.GetUI(m_audioKey_appear).Clip;
        m_As.Play();
        m_ant.Play("Appear_transition");
       
    }
    public void AnimDisappear()
    {
        m_As.clip = m_audiodata.GetUI(m_audioKey_disappear).Clip;
        m_As.Play();
        m_ant.Play("Disappear_transition");
    }    
    void OnDestroy()
    {
        PreLoad.InPreload = false;
    }

    
}

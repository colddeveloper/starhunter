﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreLoad : MonoBehaviour
{
    private static bool m_inPreload = false;
    public static bool InPreload { get => m_inPreload; set => m_inPreload = value; }

    

    public static PreLoadObject AppearPreLoad(string sceneToLoad)
    {
        GameObject obj = Instantiate(Resources.Load<GameObject>("Canvas_preload")) as GameObject;
        PreLoadObject preload = obj.GetComponent<PreLoadObject>();
        preload.Config(sceneToLoad);
        DontDestroyOnLoad(obj);
        return preload;
    }

}

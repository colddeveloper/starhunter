﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CursorMouseAimUI : MonoBehaviour
{
    [SerializeField] Image _aim;
    Controlls _controlls;
    Vector2 _mousePosition;
    
    private void Awake()
    {
        _controlls = new Controlls();
        _controlls.Player.Aim.performed += ctx => _mousePosition = ctx.ReadValue<Vector2>();
        Cursor.visible = false;
        _aim.enabled = true;
    }
    private void OnEnable()
    {
        _controlls.Enable();
    }
    private void OnDisable()
    {
        _controlls.Disable();
    }
    void Update()
    {
        _aim.transform.position = _mousePosition;
    }
}

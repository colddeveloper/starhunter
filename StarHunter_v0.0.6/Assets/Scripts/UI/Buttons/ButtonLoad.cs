﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonLoad : MonoBehaviour
{
    [SerializeField] string _scene;

    public void PressButton()
    {
        PreLoad.AppearPreLoad(_scene);
    }
}

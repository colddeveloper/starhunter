﻿using UnityEngine;
using Photon.Pun;
public class ButtonTabShowStatus : MonoBehaviour
{
    
    //This feature will no longer be implemented
    //#Este recurso não será mais implementado por falta de necessidade
    //Invés disso adicionei um contador de estrelas que fica sempre vísivel no canto inferior esquerdo da tela
    //Assim o jogador sempre vai saber quantas estrelas tem sem precisar apertar TAB

        [SerializeField] WindowStatusManager _status_TAB;
    [SerializeField] PhotonView _pv;
    int _buttonStatusPressing;
    Controlls _controlls;
    
    private void Awake()
    {
        
        _controlls = new Controlls();
        _controlls.Player.Status.performed += ctx => _buttonStatusPressing = 1;
        _controlls.Player.Status.canceled += ctx => _buttonStatusPressing = 0;
        SignalsGame.Listen_OnMyPlayerSpawn += OnPlayerSpawn;
        SignalsGame.Listen_OnDie += OnPlayerDie;
    }


    void OnPlayerSpawn(int pViewID)
    {
        if(_pv.IsMine)
        _controlls.Enable();
    }
    void OnPlayerDie(int Vid_assassin,int Vid_vitime)
    {                
        if(_status_TAB)
        _status_TAB.gameObject.SetActive(false);
        _controlls.Disable();        
    }


    private void OnEnable()
    {
        if (_pv.IsMine)
            _controlls.Enable();
    }
    private void OnDisable()
    {
        _controlls.Disable();
    }
    private void LateUpdate()
    {
        if (!_pv.IsMine) return;

        if (_buttonStatusPressing != 0 && _status_TAB.gameObject.activeSelf == false)
        {
            _status_TAB.gameObject.SetActive(true);
            _status_TAB.Config(PlayerInfo.Name, PlayerInfo.Score, PlayerInfo.Kills, PlayerInfo.Dies,false);
        }
        if (_buttonStatusPressing == 0 && _status_TAB.gameObject.activeSelf == true)
            _status_TAB.gameObject.SetActive(false);
    }
}

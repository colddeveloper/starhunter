﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonExit : MonoBehaviour
{
    public void PressExit()
    {
        Invoke("Exit", 0.5f);
    }
    void Exit()
    {
        Application.Quit();
    }
}

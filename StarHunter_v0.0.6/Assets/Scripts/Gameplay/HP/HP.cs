﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace HPpoints
{
    public class HP : UiBarr
    {
        [Header("HP VARIABLES")]
        [SerializeField] protected PhotonView _pv;
        [SerializeField] float _maxHP;
        [SerializeField] float _minHP;
        [SerializeField] float _hp;
        [SerializeField] AUDIO_GAMEPLAY _audioSpawn;
        [SerializeField] AUDIO_GAMEPLAY _audioDamage;
        [SerializeField] AUDIO_GAMEPLAY _audioDie;
        [SerializeField] AUDIO_GAMEPLAY _audioCure;
        [SerializeField] GFXKEY _gfxSPAWN;
        [SerializeField] GFXKEY _gfxDamage;
        [SerializeField] GFXKEY _gfxDie;
        [SerializeField] GFXKEY _gfxCure;
        [SerializeField] protected PhotonView _audioSourcePhotonView;

        bool _isDie;
        public bool IsDie { get => _isDie; }
        public PhotonView Pv { get => _pv; }

        private void Awake()
        {
            _minValue = _minHP;
            _maxValue = _maxHP;
            _currentValue = _hp;
            name += "_ViewID_" + _pv.ViewID;
        }

        protected virtual void Start()
        {
            SignalsAudio.Dispatch_PlayAudioGameplay(_audioSpawn, _audioSourcePhotonView.ViewID, true);
            SignalsGfx.Dispatch_InstanceGFX(_gfxSPAWN, transform.position);
        }


        public virtual bool Damage(int damage, int assassinViewID)
        {
            if (_isDie) return false;
            bool R = _pv.ViewID != assassinViewID;//I don't take damage on myself
            if (R)
                _pv.RPC("DamageRPC", RpcTarget.AllBuffered, damage, assassinViewID, _pv.ViewID);//RPC DAMAGE
                                                                                                //Signals Action
            SignalsAudio.Dispatch_PlayAudioGameplay(_audioDamage, _audioSourcePhotonView.ViewID, true);
            SignalsGfx.Dispatch_InstanceGFX(_gfxDamage, transform.position);
            //--
            if (_isDie)
            {
                SignalsAudio.Dispatch_InstanceAudioGameplay(_audioDie, transform.position, true, true);
                SignalsGfx.Dispatch_InstanceGFX(_gfxDie, transform.position);
                if (tag == "BOX") print("ADD GFX");
            }
            //--
            return R;
        }
        public virtual bool Cure(int viewIDBeneficiary, int cure)
        {
            bool R = viewIDBeneficiary == _pv.ViewID;
            if (!R) return false;
            if (_isDie) return false;
            _pv.RPC("CureRPC", RpcTarget.AllBuffered, cure, _pv.ViewID);//RPC CURE
                                                                        //Signals Action
            SignalsAudio.Dispatch_PlayAudioGameplay(_audioCure, _audioSourcePhotonView.ViewID, true);
            SignalsGfx.Dispatch_InstanceGFX(_gfxCure, transform.position);
            return true;
        }



        [PunRPC]
        protected virtual void DamageRPC(int damage, int assassinViewID, int vitimeViewID)
        {
            if (_isDie) return;
            _hp -= damage;
            if (_hp <= 0)
                Die(assassinViewID, vitimeViewID);
            _currentValue = _hp;
        }

        [PunRPC]
        protected virtual void CureRPC(int cure, int viewIdBeneficiary)
        {
            if (_isDie) return;
            _hp += cure;
            if (_hp >= _maxHP)
                _hp = _maxHP;
            _currentValue = _hp;
        }

        protected virtual void Die(int assassinViewID, int vitimeViewID)
        {
            _hp = 0;
            _isDie = true;
        }


    }
}
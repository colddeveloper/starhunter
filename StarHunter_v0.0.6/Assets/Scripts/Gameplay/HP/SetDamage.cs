﻿using UnityEngine;
using Photon.Pun;
using HPpoints;
public class SetDamage : MonoBehaviour
{
    [Header("Set only if owner setdamage object have HP script for setdamage.cs not damage in owner")]
    [SerializeField] PhotonView _pv;
    [SerializeField] HP _owner;
    [SerializeField] DAMAGE_TAG[] _setDamageInTags;
    [SerializeField] int _damage;
    public HP Owner { get => _owner; }
    public void Config(HP hpOwner, int damage)
    {
        _owner = hpOwner;
        _damage = damage;
    }
    private void Awake()
    {
        if (!_pv) _pv = GetComponent<PhotonView>();
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (!_owner) return;
        if (!_owner.Pv) return;
        GetDamage get = collision.GetComponent<GetDamage>();
        if (get == null) return;
        foreach (DAMAGE_TAG mtag in _setDamageInTags)
            if (get.CompareDamageTag(mtag))
                get.TryDamage(_damage, _owner.Pv.ViewID);
    }
}



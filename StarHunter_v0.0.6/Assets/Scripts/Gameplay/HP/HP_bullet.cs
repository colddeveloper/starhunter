﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using HPpoints;

public class HP_bullet : HP
{
    public override bool Damage(int damage, int assassinViewID)
    {
        return base.Damage(damage, assassinViewID);
    }
    protected override void Die(int assassinViewID, int vitimeViewID)
    {
        base.Die(assassinViewID, vitimeViewID);
        if (_pv.IsMine)
            PhotonNetwork.Destroy(gameObject);
    }
 }


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using HPpoints;
public class HP_player : HP
{
    [Header("Exclusive player variables")]
    [SerializeField] bool _isBot = false;
    [SerializeField] DropOnDestroy _drops;
    [SerializeField] GameObject _scoreStarPrefabDrop;
    [SerializeField] GameObject _lifePrefabDrop;
    [SerializeField] WeaponManager _weaponManager;
    protected override void Start()
    {
        base.Start();
        SignalsGame.Listen_OnGetLife += OnGetLife;        
    }
    private void OnDestroy()
    {
        SignalsGame.Listen_OnGetLife -= OnGetLife;
    }

    void OnGetLife(int viewId, int valueLife)
    {
        if (_pv.ViewID != viewId) return;
        Cure(viewId,valueLife);
    }


    protected override void Die(int assassinViewID, int vitimeViewID)
    {
        base.Die(assassinViewID, vitimeViewID);
        
        if (tag == "Player" && _pv.IsMine == false)
            PhotonView.Find(assassinViewID).GetComponent<HP_player>().RegisterKill(assassinViewID, vitimeViewID);     //#Register Players Kills   
        
        if (tag == "Player" && _pv.IsMine == true)
            RegisterDie(assassinViewID, vitimeViewID);


        //# Drops BOT
        if (tag == "BOT" && PhotonNetwork.IsMasterClient && _pv.ViewID == vitimeViewID)
        {            
            DropStarScore(_drops, Random.Range(2, 10));
            DropWeapon();
            _drops.OnDie();
            PhotonNetwork.Destroy(gameObject);
            return;
        }

             
        
    }


    [PunRPC]
    void PlayerDrops(int viewID,int starscore,int amountLifes)
    {
        DropOnDestroy drops = PhotonView.Find(viewID).GetComponent<DropOnDestroy>();
        DropStarScore(drops, starscore);
        DropLifes(drops, amountLifes);
        drops.OnDie();
    }

    void DropStarScore(DropOnDestroy drops,int amount)
    {
        for (int a = 0; a < amount; a++)
            if(a < 100)//Drop limit
            drops.AddDropInList(new DropOnDestroy.ObjectsToDrop(_scoreStarPrefabDrop, 100));        
    }
    void DropLifes(DropOnDestroy drops, int amount)
    {
        for (int a = 0; a < amount; a++)
            drops.AddDropInList(new DropOnDestroy.ObjectsToDrop(_lifePrefabDrop, 100));
    }

    void DropWeapon()
    {
        if (_weaponManager == null) return;
        if (_weaponManager.Weapons == null) return;
        foreach (Weapon w in _weaponManager.Weapons)
        {
            _drops.AddDropInList(new DropOnDestroy.ObjectsToDrop(w.GetWeaponPf, 100));
        }
    }




    public void RegisterKill(int assassinViewID, int vitimeViewID)
    {
        if (!CompareTag("Player")) return;
        if (_pv.ViewID != assassinViewID) return;        
        SignalsGame.Dispatch_OnKill(assassinViewID, vitimeViewID);
        PhotonView.Find(vitimeViewID).GetComponent<Collider2D>().enabled = false;                
    }
    void RegisterDie(int assassinViewID, int vitimeViewID)
    {
        if (!CompareTag("Player")) return;
        if (_pv.ViewID != vitimeViewID) return;        
        int amountLifesToDrop = Random.Range(0, 4);
        _pv.RPC("PlayerDrops", RpcTarget.All, _pv.ViewID, PlayerInfo.Score, amountLifesToDrop);
        SignalsGame.Dispatch_OnDie(assassinViewID, vitimeViewID);
        WindowStatus.Status(PlayerInfo.Name, PlayerInfo.Score, PlayerInfo.Kills, PlayerInfo.Dies, true);
        PhotonNetwork.Destroy(gameObject);        
    }

}

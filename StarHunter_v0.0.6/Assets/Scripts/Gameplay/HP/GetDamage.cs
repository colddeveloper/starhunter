﻿using UnityEngine;
using Photon.Pun;
using HPpoints;
public class GetDamage : MonoBehaviour
{
        [SerializeField] HP _hp;
        [SerializeField] DAMAGE_TAG[] _myDamageTags;
        PhotonView _owner;
        public PhotonView Owner { get => _owner; set => _owner = value; }
        private void Awake()
        {
            if (_hp.Pv) _owner = _hp.Pv;
        }

        public void SetMyOwner(int viewId)
        {
            _hp.Pv.RPC("SetMyOwnerRPC", RpcTarget.AllBuffered, viewId);
        }
        [PunRPC]
        private void SetMyOwnerRPC(int id)
        {
            _owner = PhotonView.Find(id);
        }
        public bool CompareDamageTag(DAMAGE_TAG tag)
        {
            bool R = false;
            foreach (DAMAGE_TAG t in _myDamageTags) if (t == tag) R = true;
            return R;
        }
        public bool TryDamage(int damage, int idAssassingViewID)//idAssassingViewID = View ID do personagem que desfere o dano
        {            
            if (_owner.ViewID == idAssassingViewID) return false;
            bool R = _hp.Damage(damage, idAssassingViewID);            
            return R;
        }
}   



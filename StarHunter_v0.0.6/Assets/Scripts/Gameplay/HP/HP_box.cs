﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using HPpoints;
public class HP_box : HP
{
    [SerializeField] DropOnDestroy _destroyOnDrop;
    protected override void Die(int assassinViewID, int vitimeViewID)
    {
        _destroyOnDrop.OnDie();
        base.Die(assassinViewID, vitimeViewID);        
        SignalsGame.Dispatch_RemoveBox(gameObject);
    }
    [PunRPC]
    protected override void DamageRPC(int damage, int assassinViewID, int vitimeViewID)
    {
        base.DamageRPC(damage, assassinViewID, vitimeViewID);
    }
}



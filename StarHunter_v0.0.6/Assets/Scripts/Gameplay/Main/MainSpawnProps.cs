﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class MainSpawnProps : MonoBehaviour
{
    [SerializeField] GameConfig _config;
    [SerializeField] PhotonView _pv;
    [SerializeField] GameObject _propPf;
    List<GameObject> _propList;
    int _maxProp;
    Transform _propsContent;
    private void Awake()
    {
        _propsContent = new GameObject("ContentProps").transform;
        _propsContent.position = Vector2.zero;
        _propList = new List<GameObject>();
        _maxProp = (int) ( (_config.Area.x * _config.Area.y) * 0.025f );//Max box Spawn by area size
        CheckAddProp();
        //#signals
        SignalsGame.Listen_OnRemoveBox += RemoveBox;
    }
    private void OnDestroy()
    {
        SignalsGame.Listen_OnRemoveBox -= RemoveBox;
    }
    private void RemoveBox(GameObject box)
    {
        if (!PhotonNetwork.IsMasterClient) return;
        _propList.Remove(box);
        PhotonNetwork.Destroy(box);
        CheckAddProp();
   }
    private void CheckAddProp()
    {
        if (PhotonNetwork.IsMasterClient == false) return;
        if (addingProp) return;
        if (_propList.Count < _maxProp)
        {
            StartCoroutine("AddProp",_propPf);
        }
    }


    bool addingProp = false;
    private IEnumerator AddProp(GameObject propAdd)
    {
        addingProp = true;
        yield return new WaitForSecondsRealtime(Random.Range(1, _config.MaxSecondsToAddProp+1));
        Vector2 ps = _config.GetRandomPositionByArea();
        GameObject prop = PhotonNetwork.Instantiate("Prefabs/"+ propAdd.name,ps, propAdd.transform.rotation);
        prop.transform.SetParent(_propsContent);
        _propList.Add(prop);
        addingProp = false;
        CheckAddProp();
        
    }

 }

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateArea : MonoBehaviour
{

    [SerializeField] Sprite _spritePointLight;
    [SerializeField] GameConfig _config;
    

    private void Awake()
    {
        GameObject decoratorBack = new GameObject("background_stars");
        decoratorBack.transform.position = Vector3.zero;

        Vector2 _area = _config.Area;
        Vector2 _size = _config.RangeSizeDecoratorStar;        
        float numStarsDecorator = _area.x * _area.y;
        for (int i = 0; i < (int)numStarsDecorator; i++)
        {
            float rnScale = Random.Range(_size.x, _size.y);
            Vector2 rnPosition = new Vector2( Random.Range(-_area.x,_area.x) , Random.Range(-_area.y, _area.y));
            GameObject decorator = new GameObject("decorator_star_" + i);
            decorator.transform.SetParent(decoratorBack.transform);
            decorator.transform.position = rnPosition;
            decorator.transform.localScale = new Vector2(rnScale, rnScale);
            SpriteRenderer sr = decorator.AddComponent<SpriteRenderer>();
            sr.sprite = _spritePointLight;
            decorator.layer = 12;
        }
        
    }

}

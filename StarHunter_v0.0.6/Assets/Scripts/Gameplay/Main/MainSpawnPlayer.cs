﻿using System.Collections;
using UnityEngine;
using Photon.Pun;
using TMPro;
public class MainSpawnPlayer : MainNetManager
{    
    [SerializeField] GameObject _player;
    [SerializeField] GameObject _gameplayScenario;
    [SerializeField] TextMeshProUGUI _tmpRestartCountdown;
    [SerializeField] PhotonView _pv;    
    Vector2 positionAdd;   
    private void Awake()
    {
        
        SignalsGame.Listen_OnRestart += OnRestartSpawnPlayer;
       
    }
    private void OnDestroy()
    {
        SignalsGame.Listen_OnRestart -= OnRestartSpawnPlayer;
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        _gameplayScenario.SetActive(true);
         SpawnPlayer();
        
    }
    
    private void SpawnPlayer()
    {        
        positionAdd = _config.GetRandomPositionByArea();
        PlayerInfo.ScoreReset();        
        GameObject pl = PhotonNetwork.Instantiate("Prefabs/" + _player.name, positionAdd, _player.transform.rotation, 0);
        PlayerInfo.ViewID = pl.GetComponent<PhotonView>().ViewID;
        SignalsGame.Dispatch_OnMyPlayerSpawn(pl.GetComponent<PhotonView>().ViewID);
        SignalsGame.Dispatch_OnGetScore(PlayerInfo.ViewID, 0);
    }
    private void OnRestartSpawnPlayer()
    {
        StartCoroutine(countdownToRestart());
    }
    IEnumerator countdownToRestart()
    {
        for(int i = 5; i >= 0; i--)
        {
            yield return new WaitForSecondsRealtime(1);
            _tmpRestartCountdown.text = i.ToString();
        }
        _tmpRestartCountdown.text = string.Empty;
        SpawnPlayer();
    }

  
}

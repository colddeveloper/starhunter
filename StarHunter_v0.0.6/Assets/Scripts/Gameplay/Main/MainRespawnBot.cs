﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class MainRespawnBot : MonoBehaviour
{
    [SerializeField] PhotonView _pv;
    [SerializeField] GameObject _BotPf;
    [SerializeField] GameConfig _config;
    [SerializeField] Vector2 _randomTimeWaitToAddBot = new Vector2(10,30);
    [SerializeField] bool _spawnBotInGame = true;

    private void Start()
    {
        if (_spawnBotInGame == false) return;
        if(PhotonNetwork.IsMasterClient)
        StartCoroutine(RespawnBot());
    }

    IEnumerator RespawnBot()
    {
        yield return new WaitForSeconds(Random.Range(_randomTimeWaitToAddBot.x, _randomTimeWaitToAddBot.y+1));
        PhotonNetwork.Instantiate("Prefabs/" + _BotPf.name, _config.GetRandomPositionByArea(), _BotPf.transform.rotation);
        Start();
    }
}

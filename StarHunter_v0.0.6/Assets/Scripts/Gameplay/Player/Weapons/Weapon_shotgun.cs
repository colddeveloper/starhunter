﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class Weapon_shotgun : Weapon
{
    [Header("Exclusive Variables for this")]
    [SerializeField] int _numBulletsOnshot = 5;
    [SerializeField] float _spreadRange = 5;



    protected override bool Fire()
    {
        bool r = base.Fire();
        if (!r) return false;
        //float rt = _spreadRange * -1;
        for (int i = 0; i < _numBulletsOnshot; i++)
        {            
            Bullet bullet = base.AddBullet();
            Quaternion rt = transform.rotation;
            rt = Quaternion.AngleAxis(_spreadRange * (i - (_numBulletsOnshot / 2)),transform.forward);
            bullet.transform.rotation = rt * transform.rotation;
            //bullet.transform.Rotate(0, 0, rt);            
            //rt += _spreadRange / _numBulletsOnshot;
        }
        return true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.InputSystem;
using HPpoints;

public class Weapon : MonoBehaviour
{
    [SerializeField] protected WeaponParameters _dataParameter;
    
    [Header("This is Necessary only for check drop item")]
    [SerializeField] protected GameObject _getWeaponPf;

    //---------------------------------------
    protected PhotonView _pv;
    protected HP _hp;
    protected int _amount;
    protected float _shootkeyPressing = 0;
    //---------------------------------------
    Controlls _controlls;
    WeaponManager _weaponManager;
    //---------------------------------------
    public WeaponParameters Data { get => _dataParameter; set => _dataParameter = value; }    
    public WeaponManager MyWeaponManager { get => _weaponManager;}
    public int Amount { get => _amount; }
    public GameObject GetWeaponPf { get => _getWeaponPf; }
    //------------------------------------------
    bool reloadedCompleted = false;
    bool _equiped;

    protected virtual void Awake()
    {
        _pv = GetComponent<PhotonView>();
        _hp = GetComponent<HP>();

        
        _controlls = new Controlls();
        _controlls.Player.Shoot.performed += ctx => _shootkeyPressing = ctx.ReadValue<float>();
        _controlls.Player.Shoot.canceled += ctx => _shootkeyPressing = 0;        
    }
    private void OnEnable()
    {
        if (_weaponManager && _weaponManager.Isbot) return;
        if (_pv.IsMine)        
            _controlls.Enable();       
    }
    
    public void Equiped()
    {
        T = 0;
        reloadedCompleted = false;
        inCooldown = true;
        _equiped = true;
        enabled = _equiped;
        ConfigVisionCamera();        
        SignalsGame.Dispatch_OnWeaponEquip(this);
    }

    public void Unequiped()
    {
        
        _equiped = false;
        enabled = _equiped;
    }
       

    void ConfigVisionCamera()
    {
        if (_dataParameter != null && _pv.IsMine && _weaponManager.Isbot == false)
            SignalsGame.Dispatch_ChangeZoomCamera(_dataParameter.Vision);
    }

    private void OnDisable()
    {
        if (_pv.IsMine)
            _controlls.Disable();
    }

    public void Config(WeaponParameters data,WeaponManager weaponmanager )
    {
        if (_pv.IsMine == false) return;
        _dataParameter = data;
        _weaponManager = weaponmanager;
        _amount = data.MaxBullets;
        _weaponManager.UpdateAmount();//Update Amount in HUD
        ConfigVisionCamera();
        if (_weaponManager.Isbot) _controlls.Disable();
    }

    protected virtual void OnKeyShot()
    {
        if(!inCooldown && _pv.IsMine) Fire();
    }
    public void RemoteFire()//#used by AI
    {
        OnKeyShot();
    }
    protected virtual bool Fire()
    {
        bool R = _amount > 0;
        if (!R) return false;//Check If Have Bullets amount to Fire        
        //---------------------------->
        DecremmentMunition();
        T = 0;
        AddBullet();
        reloadedCompleted = false;
        _weaponManager.PlayAudio(_dataParameter.AudioFire,true);
        _weaponManager.AddGfx(_dataParameter.GfxFire, transform.position);
        SignalsGame.Dispatch_OnFire(this);
        return true;
        
    }

    protected virtual Bullet AddBullet()
    {
        GameObject instance = PhotonNetwork.Instantiate("Prefabs/Bullets/" + _dataParameter.BulletPf.name, _weaponManager.PointShot.position, transform.rotation);
        Bullet bullet = instance.GetComponent<Bullet>();                
        bullet.GetComponent<SetDamage>().Config(_hp, _dataParameter.Damage);
        bullet.GetComponent<GetDamage>().SetMyOwner(_pv.ViewID);
        bullet.Config(_dataParameter.BulletSpeed, _dataParameter.Range);
        return bullet;
    }

    protected virtual void DecremmentMunition()
    {
        _amount--;
        _weaponManager.OnDecremmentMunition();
        if (_amount <= 0) _weaponManager.RemoveWeapon(this);
        
    }

    float T = 0;
    bool inCooldown = true;
    protected virtual void Update()
    {
        if (!_pv.IsMine) return;
        
        //#Reload / Cooldown / Firerate

        inCooldown = T < _dataParameter.FireRate;
        if (!inCooldown) T = _dataParameter.FireRate; else T += Time.deltaTime; //cooldown weapon
        if (_shootkeyPressing != 0) OnKeyShot();


        
        if(T >= _dataParameter.FireRate && reloadedCompleted == false)
        {
            reloadedCompleted = true;
            if(_weaponManager.Isbot == false) _weaponManager.PlayAudio(AUDIO_GAMEPLAY.WEAPON_RELOAD,false);
            SignalsGame.Dispatch_OnReloadedWeapon(this);
        }

    }

    public void AddMunition()
    {
        _amount += _dataParameter.MaxBullets;
        _weaponManager.UpdateAmount();
    }



}

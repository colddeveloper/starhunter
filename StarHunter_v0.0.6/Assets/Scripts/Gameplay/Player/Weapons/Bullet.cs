﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class Bullet : MonoBehaviour
{    
    [SerializeField] Rigidbody2D _rg;
    [SerializeField] PhotonView _pv;
    [SerializeField] float _speed;
    [SerializeField] float _maxDistance;
    [SerializeField] GameObject[] _visibleSprites;
    //[SerializeField] Collider2D _collider;
    public float Speed { set => _speed = value; }
    public float MaxDistance { set => _maxDistance = value; }

    Vector2 _startPosition;
    private void Awake()
    {        
        //_collider.enabled = false;
        _startPosition = transform.position;
    }

    private void Start()
    {
        Invoke("Visible", 0.01f);
    }
    void Visible()
    {
        foreach (GameObject str in _visibleSprites) str.layer = 12;
    }

    public void Config(float speed , float range)
    {        
        _speed = speed;
        _maxDistance = range;
        //_collider.enabled = true;
    }

    private void FixedUpdate()
    {
        if (!_pv.IsMine) return;
        _rg.velocity = transform.right * _speed * Time.fixedDeltaTime;//#move
        if (Vector2.Distance(_startPosition, transform.position) >= _maxDistance) PhotonNetwork.Destroy(gameObject);//_pv.RPC("DestroyRPC", RpcTarget.All);//#Destroy by range
    }

    /*[PunRPC]
    void DestroyRPC()
    {
        Destroy(gameObject);
    }*/

}

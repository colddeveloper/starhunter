﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class Weapon_use : Weapon
{
    protected override Bullet AddBullet()
    {
        Bullet bullet = base.AddBullet();
        float rt = Random.Range(-3, 3);
        bullet.transform.Rotate(0, 0, rt);
        return bullet;
    }    
        
}

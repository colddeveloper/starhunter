﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.InputSystem;
public class WeaponManager : MonoBehaviour
{
    //Serialized
    
    [SerializeField] WeaponsStorageData _storage;
    [SerializeField] GameObject _canvasWeaponHUD_pf;
    [SerializeField] Transform _pointShot;
    [SerializeField] Movment _movment;
    [SerializeField] PhotonView _audioSourcePhotonView;
    [SerializeField] AUDIO_GAMEPLAY _audioEquipWeapon;
    //Private
    bool _isBot;
    List<Weapon> _weapons;
    int _indexEquiped;
    WeaponHudManager _weaponHudManager;
    PhotonView _pv;
    Controlls _controlls;
    //Public
    public List<Weapon> Weapons { get => _weapons; }
    //public WeaponHudManager MyWeaponHudManager { get => _weaponHudManager; }
    public Transform PointShot { get => _pointShot; }
    public PhotonView Pv { get => _pv; }
    public Movment MyMovment { get => _movment; }
    

    public bool Isbot { get => _isBot; }
    private void Awake()
    {
        _isBot = CompareTag("BOT");
        _pv = GetComponent<PhotonView>();
        if (_pv.IsMine == false) return;
        if (_isBot) return;

        SetControlls();
        AddWeaponHUD();
    }
    private void Start()
    {

        if (_pv.IsMine == false) return;        
        _weapons = new List<Weapon>();
        //Add Initial Weapon Weapon
        AddWeaponScript(WEAPONTYPE.PISTOL);        
        //---------Signals
        SignalsGame.Listen_OnGetWeapon += OnGetWeapon;
    }

    private void OnDestroy()
    {
        SignalsGame.Listen_OnGetWeapon -= OnGetWeapon;
    }

    void OnGetWeapon(int viewId,WEAPONTYPE _type)
    {
        if (viewId != _pv.ViewID) { return; }

        //--------------------------------------Check If this Weapon exists in storage
        Weapon weaponExistsInStorage = null;
        foreach(Weapon w in _weapons)        
            if(w.Data.WeaponType == _type)            
                weaponExistsInStorage = w;

        //If not exist weapon Add else AddMunition
        if (!weaponExistsInStorage)
            AddWeaponScript(_type);
        else
            weaponExistsInStorage.AddMunition();
    }


    void SetControlls()
    {
        _controlls = new Controlls();
        _controlls.Player.EquipWeapon_1.performed += ctx => EquipWeapon(0);
        _controlls.Player.EquipWeapon_2.performed += ctx => EquipWeapon(1);
        _controlls.Player.EquipWeapon_3.performed += ctx => EquipWeapon(2);
        _controlls.Player.EquipWeapon_4.performed += ctx => EquipWeapon(3);
    }

    private void OnEnable()
    {
        if (_isBot) return;
        if (_pv.IsMine == false) return;
        _controlls.Enable();
    }
    private void OnDisable()
    {
        if (_isBot) return;
        if (_pv.IsMine == false) return;
        _controlls.Disable();
    }

    void EquipWeapon(int index)
    {
        if (index > _weapons.Count - 1) return;//Player not have this weapon in your storage to equip
        _indexEquiped = index;
        
        if (_weaponHudManager)
        {
            _weaponHudManager.UpdateHUD();
            _weaponHudManager.UpdateAmount();
            PlayAudio(_audioEquipWeapon,false);
        }

        EnableWeaponEquiped();
    }

    void AddWeaponHUD()
    {
        if (_pv.IsMine == false) return;
        GameObject canvas = Instantiate(_canvasWeaponHUD_pf);
        
        _weaponHudManager = canvas.GetComponent<WeaponHudManager>();
        _weaponHudManager.Config(this);
    }

    public void AddWeaponScript(WEAPONTYPE weaponType)
    {
        if (_pv.IsMine == false) return;
        Weapon we = _storage.GetWeaponByType(weaponType);
        Weapon weaponAdded = (Weapon)gameObject.AddComponent(we.GetType());
        weaponAdded.Config(we.Data,this);
        _weapons.Add(weaponAdded);        
        EquipWeapon(_weapons.Count-1);    
    }

    public void RemoveWeapon(Weapon w)
    {
        if (_pv.IsMine == false) return;
        _weapons.Remove(w);
        Destroy(w);        
        EquipWeapon(_weapons.Count-1);
    }

    private void EnableWeaponEquiped()
    {
        if (_pv.IsMine == false) return;
        int i = 0;
        foreach(Weapon we in _weapons)
        {            
            if (i == _indexEquiped) we.Equiped(); else we.Unequiped();
            i++;
        }
    }


    public void FireWhithCurrentWeapon()//Used for Ai
    {
        _weapons[_indexEquiped].RemoteFire();
    }


    public Weapon GetCurrentWeapon()
    {
        return _weapons[_indexEquiped];
    }

    //#HUD CALLS
    public void UpdateAmount()
    {
        if(_weaponHudManager)
        _weaponHudManager.UpdateAmount();
    }
    public void OnDecremmentMunition()
    {
        if (_weaponHudManager)
            _weaponHudManager.OnDecremmentMunition();
    }

    public void PlayAudio(AUDIO_GAMEPLAY key,bool online)//Called in Weapon To Fire
    {        
        SignalsAudio.Dispatch_PlayAudioGameplay(key, _audioSourcePhotonView.ViewID, online);
    }
    public void AddGfx(GFXKEY key,Vector2 ps)//Called in Weapon To Fire
    {
        SignalsGfx.Dispatch_InstanceGFX(key, ps);
    }

}

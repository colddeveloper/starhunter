﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Photon.Pun;
public class Aim : MonoBehaviour
{
    Controlls controlls;
    Vector3 mousePosition;
    PhotonView m_pv;
    [SerializeField] Camera _myCamera;
    bool _isBot;
    private void Awake()
    {
        _isBot = CompareTag("BOT");
        m_pv = GetComponent<PhotonView>();
        if (_isBot) return;
        controlls = new Controlls();
        controlls.Player.Aim.performed += ctx => mousePosition = ctx.ReadValue<Vector2>();
        controlls.Player.Aim.canceled += ctx => mousePosition = Vector2.zero;
        
    }
    private void OnEnable()
    {
        if (_isBot) return;
        controlls.Enable();

    }
    private void OnDisable()
    {
        if (_isBot) return;
        controlls.Disable();
    }


    public void SetWorldSpacePositionTarget(Vector2 ps)
    {
        mousePosition = _myCamera.WorldToScreenPoint(ps);
    }


    void Update()
    {
        if (!m_pv.IsMine) return;
        Vector3 dir = mousePosition - _myCamera.WorldToScreenPoint(transform.position);
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Break : MonoBehaviour
{    
    [SerializeField] Rigidbody2D m_rg;
    [SerializeField] float m_breakValue;
    void Awake()
    {
        if (!m_rg) m_rg = GetComponent<Rigidbody2D>();        
    }    
    void FixedUpdate()
    {
        Vector2 vl = m_rg.velocity;
        vl *= m_breakValue;
        m_rg.velocity = vl;
    }
}

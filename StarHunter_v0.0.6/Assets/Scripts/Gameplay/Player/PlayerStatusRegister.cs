﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class PlayerStatusRegister : MonoBehaviour
{
    [SerializeField] PhotonView _pv;
    private void Awake()
    {
        if (!_pv) _pv = GetComponent<PhotonView>();
        SignalsGame.Listen_OnDie += OnDie;
        SignalsGame.Listen_OnKill += OnKill;
        SignalsGame.Listen_OnGetScore += OnGetStar;
    }
    private void OnDestroy()
    {
        SignalsGame.Listen_OnDie -= OnDie;
        SignalsGame.Listen_OnKill -= OnKill;
        SignalsGame.Listen_OnGetScore -= OnGetStar;
    }
    private void OnDie(int viewID_assassin,int viewID_vitime)
    {
        if(_pv.ViewID == viewID_vitime)
            PlayerInfo.IncremmentDies();
    }
    private void OnKill(int viewID_assassin, int viewID_vitime)
    {
        if (_pv.ViewID == viewID_assassin)
            PlayerInfo.IncremmentKills();
    }
    private void OnGetStar(int viewID,int scoreValue)
    {
        if (_pv.ViewID == viewID)
            PlayerInfo.IncremmentScore(scoreValue);
    }
}

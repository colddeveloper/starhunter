﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using Photon.Pun;
using UnityEngine.InputSystem;
public class Movment:MonoBehaviour
{
    #region Serialized
    [SerializeField] bool _isBot = false;
    [SerializeField] Break m_break;    
    [SerializeField] float m_speed;
    [SerializeField] float m_maxSpeed;
    [SerializeField] float m_maxSpeedBreak;
    [SerializeField] GameConfig _config;
    #endregion
    //--
    Rigidbody2D m_rigidbody;
    PhotonView m_pv;
    Controlls controlls;    
    Vector2 m_moveInput;
    Vector2 m_moveDirection;
    

    protected void Awake()
    {            
        if (!m_break) m_break = GetComponent<Break>();
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_pv = GetComponent<PhotonView>();
        //-Controlls
        if (_isBot) return;
        controlls = new Controlls();
        controlls.Player.Move.performed += ctx => m_moveInput = ctx.ReadValue<Vector2>();
        controlls.Player.Move.canceled += ctx => m_moveInput = Vector2.zero;
        
    }
    public void OnEnable()
    {
        if (_isBot) return;
        controlls.Enable();
    }
    public void OnDisable()
    {
        if (_isBot) return;
        controlls.Disable();
    }
    //--
    private void FixedUpdate()
    {

        if (!m_pv.IsMine) return;
        
        
        m_moveDirection = m_moveInput * m_speed * Time.fixedDeltaTime;
        m_rigidbody.AddForce(m_moveDirection);//move        
        Vector2 vl = m_rigidbody.velocity;
        if (vl.magnitude > m_maxSpeed)
        {
            vl *= m_maxSpeedBreak;
            m_rigidbody.velocity = vl;
        }
        bool breaking = m_moveInput.magnitude == 0;
        if (m_break.enabled != breaking) m_break.enabled = breaking;
        
        //Scenario Limits
        NoExitScenarioLimits();

    }

    void NoExitScenarioLimits()
    {        
        Vector2 clampPS = m_rigidbody.position;
        clampPS.x = Mathf.Clamp(clampPS.x, -_config.Area.x, _config.Area.y);
        clampPS.y = Mathf.Clamp(clampPS.y, -_config.Area.y, _config.Area.y);
        m_rigidbody.position = clampPS;
    }


    public void ModfyParameterByFator(float fatorModfy)
    {
        m_speed *= fatorModfy;
        m_maxSpeed *= fatorModfy;
        m_maxSpeedBreak *= fatorModfy;
    }


    public void StartMovment(Vector2 mov)
    {
        m_moveInput = mov;
    }
    public void StopMovment()
    {
        m_moveInput = Vector2.zero;
    }

    public void AddBackImpulse(float force)
    {
        m_rigidbody.AddForce(-transform.right * force);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class PlayerCameraManager : MonoBehaviour
{
    [SerializeField] Camera _camera;    
    [SerializeField] PhotonView _pv;
    [SerializeField] float _speedFollow = 8;
    [SerializeField] float _defaultZ = -20;
    [SerializeField] float _zoom = 5;



    private void Awake()
    {
        
        if (_pv.IsMine)
        {
            //_camera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
            _camera.transform.SetParent(null);
            _camera.gameObject.SetActive(true);
            SignalsGame.Listen_ChangeZoomCamera += OnChangeZoom;
        }
        else
            Destroy(this);
    }
    private void OnDestroy()
    {
        if(_camera && _camera.transform.parent == null)
        Destroy(_camera.gameObject);
        SignalsGame.Listen_ChangeZoomCamera -= OnChangeZoom;
    }
    private void OnChangeZoom(float zoom)
    {        
        _zoom = zoom;
    }

    private void FixedUpdate()
    {
       
        Move();
        Zoom();        
    }
    void Zoom()
    {
        float zoom = Mathf.Lerp(_camera.orthographicSize,_zoom, _speedFollow * Time.fixedDeltaTime);
        _camera.orthographicSize = zoom;
    }
    void Move()
    {
        Vector3 ps = Vector2.Lerp(_camera.transform.position, _pv.transform.position, _speedFollow * Time.fixedDeltaTime);
        ps.z = _defaultZ;
        _camera.transform.position = ps;
    }


}

﻿using UnityEngine;
using Photon.Pun;
public class Collect : MonoBehaviour
{
    [SerializeField] PhotonView _pv;
    [SerializeField] AUDIO_GAMEPLAY _audio;
    [SerializeField] GFXKEY _Gfxkey;
    [SerializeField] GameConfig _config;

    private void Awake()
    {
        if (_config.OutScenario(transform.position)) Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player") &&  !collision.CompareTag("BOT")) return; 
        
        OnCollect(collision.GetComponent<PhotonView>());
        SignalsAudio.Dispatch_InstanceAudioGameplay(_audio, transform.position,true,true);
        SignalsGfx.Dispatch_InstanceGFX(_Gfxkey, transform.position);
        if (!_pv.IsMine) return;
        PhotonNetwork.Destroy(gameObject);
    }
    protected virtual void OnCollect(PhotonView playercollider)
    {

    }
}

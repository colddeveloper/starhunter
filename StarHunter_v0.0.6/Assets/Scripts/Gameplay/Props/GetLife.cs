﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class GetLife : Collect
{
    [Header("Exclusives Variables this")]
    [SerializeField] int _amountLifePoints = 10;
    protected override void OnCollect(PhotonView playerPV)
    {     
        base.OnCollect(playerPV);
        if (playerPV.IsMine)
            SignalsGame.Dispatch_OnGetLife(playerPV.ViewID, _amountLifePoints);
    }
}

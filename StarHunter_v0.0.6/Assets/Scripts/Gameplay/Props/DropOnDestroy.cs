﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Photon.Pun;

public class DropOnDestroy : MonoBehaviour
{
    [SerializeField] List<ObjectsToDrop> _objectsToDrop_list;
    [SerializeField] float _spreadPosition = 0.2f;
    
    public void AddDropInList(ObjectsToDrop objectToDrop)
    {
        _objectsToDrop_list.Add(objectToDrop);
    }
    

    public void OnDie()
    {
        if (!PhotonNetwork.IsMasterClient) return;
        _spreadPosition += _objectsToDrop_list.Count * 0.01f;
        foreach (ObjectsToDrop drop in _objectsToDrop_list)
        {
            GameObject objtToDrop = drop.CheckIfDrop();
            if (objtToDrop != null)
            {
                Vector2 rndPosition = transform.position;
                rndPosition.x += UnityEngine.Random.Range(-_spreadPosition,_spreadPosition);
                rndPosition.y += UnityEngine.Random.Range(-_spreadPosition, _spreadPosition);
                PhotonNetwork.Instantiate("Prefabs/"+objtToDrop.name, rndPosition, objtToDrop.transform.rotation);
            }
        }
    }






    [Serializable]
    public class ObjectsToDrop
    {
        [SerializeField] string propName;//#only for organization
        [SerializeField] GameObject _prefab;
        [SerializeField] float _percentToDrop;


        public ObjectsToDrop(GameObject prefab,float percentToDrop)
        {
            _percentToDrop = percentToDrop;
            _prefab = prefab;
        }

        public GameObject CheckIfDrop()
        {
            GameObject R = null;
            if (_percentToDrop >= UnityEngine.Random.Range(0, 101))
                R = _prefab;
         
            return R;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class GetWeapon : Collect
{
    [Header("Exclusives Variables this")]
    [SerializeField] WEAPONTYPE _weapon;
    protected override void OnCollect(PhotonView playerPV)
    {  
        base.OnCollect(playerPV);
        if (playerPV.IsMine)
            SignalsGame.Dispatch_OnGetWeapon(playerPV.ViewID, _weapon);
    }
}

﻿using UnityEngine;
using Photon.Pun;
public class GetStar : Collect
{
 
    protected override void OnCollect(PhotonView playerPV)
    {        
        base.OnCollect(playerPV);
        if (playerPV.CompareTag("BOT")) return;
        if (playerPV.IsMine)
            SignalsGame.Dispatch_OnGetScore(playerPV.ViewID, 1);
    }
  

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace StageMachineAI
{
    public class StageFollow : StageAi
    {

        [Header("Exclusive Variables")]
        [SerializeField] float _speedIA;
        [SerializeField] Movment _movment;
        [SerializeField] Aim _aim;

        protected override void Awake()
        {
            base.Awake();
            _movment.ModfyParameterByFator(_speedIA);
        }

        protected override bool Update()
        {
            bool active = base.Update();
            if (!active) return false;
            
            if (StageMashineM.CurrentTarget)
            {
                _movment.StartMovment(_movment.transform.right);
                _aim.SetWorldSpacePositionTarget(StageMashineM.CurrentTarget.transform.position);
            }
            return true;
        }

        public override void DisableStage()
        {
            base.DisableStage();
            _movment.StopMovment();
        }

    }
}

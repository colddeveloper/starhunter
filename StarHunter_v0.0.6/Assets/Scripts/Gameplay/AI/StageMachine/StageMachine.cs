﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace StageMachineAI
{
    public enum StageType { PATROL,FOLLOW,ATTACK }

    public class StageMachine : Vision
    {        
        protected List<StageAi> _stagesList;
        protected StageAi _currentStage;

        protected override void Awake()
        {
            base.Awake();
            //-------------------------------------
            _stagesList = new List<StageAi>();
            StageAi[] stagesFind = gameObject.GetComponents<StageAi>();
            foreach (StageAi s in stagesFind)
            {
                _stagesList.Add(s);
                s.StageMashineM = this;
            }
            //-------------------------------------


            ChangeStage(StageType.PATROL);
        }

        protected virtual void Start()
        {

        }
        
        public void ChangeStage(StageType newStage)
        {
            _currentStage = null;
            foreach (StageAi stage in _stagesList)
            {
                stage.DisableStage();
                if (stage.MyStageType == newStage)
                    _currentStage = stage;
            }
            
            if (_currentStage == null) return;
            _currentStage.EnableStage();
            
        }

        protected virtual void FixedUpdate()
        {

        }
      
    }

  
}

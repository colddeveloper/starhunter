﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace StageMachineAI
{
    public class StageMachine_DefaultAi : StageMachine
    {
        [SerializeField] float _minDistanceToAttack;
        [SerializeField] WeaponManager _weaponManager;


        [Header("Chance to add others weapon for BOT")]
        [SerializeField] AddRandonWeapon[] _chanceToAddWeaponOnStart;




       


        protected override void Start()
        {
            //Add or not Weapons to bot
            foreach (AddRandonWeapon r in _chanceToAddWeaponOnStart)
                r.CheckIfAddWeapon(_weaponManager);
        }


        protected override void FixedUpdate()
        {

            if (_weaponManager.Pv.IsMine == false) return;

            base.FixedUpdate();

            //While change stage?


            switch (_currentStage.MyStageType)
            {
                case StageType.PATROL:

                        PatrolChanges();

                    break;

                case StageType.FOLLOW:

                        FollowChanges();

                    break;

                case StageType.ATTACK:

                        AttackChanges();

                    break;

            }

        }
        void PatrolChanges()
        {
            if (_currentTarget != null)
                ChangeStage(StageType.FOLLOW);
        }
        void FollowChanges()
        {
            if (_distanceTarget <= _minDistanceToAttack)
                ChangeStage(StageType.ATTACK);
            ChangeToPatrolIfTargetIsNull();
        }
        void AttackChanges()
        {
            if (_distanceTarget > _minDistanceToAttack)
                ChangeStage(StageType.PATROL);
            ChangeToPatrolIfTargetIsNull();
        }

        void ChangeToPatrolIfTargetIsNull()
        {
            if (CurrentTarget == null && !chanching)
                StartCoroutine("YieldChangeStage", StageType.PATROL);
            if (CurrentTarget != null && chanching)
            {
                StopCoroutine("YieldChangeStage");
                chanching = false;
            }
        }


        bool chanching = false;
        IEnumerator YieldChangeStage(StageType stage)
        {
            chanching = true;
            yield return new WaitForSeconds(1);
            ChangeStage(stage);
            chanching = false;
        }





        [Serializable]
        public class AddRandonWeapon
        {
            [SerializeField] string _nameOnlyOrganization;
            [SerializeField] WEAPONTYPE _weaponType;
            [SerializeField] float _percentToAdd;
            public WEAPONTYPE WeaponType { get => _weaponType; }
            public void CheckIfAddWeapon(WeaponManager wm)
            {
                if (UnityEngine.Random.Range(0, 101) <= _percentToAdd)
                {
                    wm.AddWeaponScript(_weaponType);
                }
            }
        }

    }
}

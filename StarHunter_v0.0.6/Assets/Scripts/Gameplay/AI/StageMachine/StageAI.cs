﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace StageMachineAI
{
    [Serializable]
    public class StageAi:MonoBehaviour
    {
        [SerializeField] string _description;                   
        [SerializeField] protected StageType _stageType;        
        [SerializeField] protected bool _isEnable;

        protected StageMachine _stageMachine;

        public StageType MyStageType { get => _stageType;}
        public StageMachine StageMashineM { get => _stageMachine; set => _stageMachine = value; }
        



        protected virtual void Awake()
        {

        }

        protected virtual void Start()
        {

        }

        protected virtual bool Update()
        {
            return _isEnable;
        }

        public virtual void EnableStage()
        {
            _isEnable = true;
        }
        public virtual void DisableStage()
        {
            _isEnable = false;
        }


        

    }
}

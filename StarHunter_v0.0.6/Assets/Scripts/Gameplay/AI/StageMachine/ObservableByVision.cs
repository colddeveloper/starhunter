﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObservableByVision : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {        
        StageMachineAI.Vision vision = collision.GetComponent<StageMachineAI.Vision>();        
        if (!vision) return;
        vision.AddObserveObject(this);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        StageMachineAI.Vision vision = collision.GetComponent<StageMachineAI.Vision>();
        if (!vision) return;
        vision.RemoveObserveObject(this);
    }
}

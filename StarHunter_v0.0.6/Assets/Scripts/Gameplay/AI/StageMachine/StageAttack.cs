﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace StageMachineAI
{
    public class StageAttack : StageAi
    {
        [Header("Exclusive Variables")]
        [SerializeField] Aim _aim;
        [SerializeField] Movment _movement;
        [SerializeField] WeaponManager _weaponManager;        
        [SerializeField] float _minDistanceToRetreat;//Recuar, Recue, Back , Retreat
        protected override bool Update()
        {
            bool active = base.Update();
            if (!active) return false;
            if (StageMashineM.CurrentTarget != null)
            _aim.SetWorldSpacePositionTarget(StageMashineM.CurrentTarget.transform.position);
            _weaponManager.FireWhithCurrentWeapon();


            if (_stageMachine.DistanceTarget <= _minDistanceToRetreat)//Retreat when player approaches
                _movement.StartMovment(transform.right * -1);
            else
                _movement.StopMovment();

            return true;
        }


    }
}

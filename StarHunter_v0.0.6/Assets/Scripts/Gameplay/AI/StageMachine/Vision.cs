﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace StageMachineAI
{
    public class Vision : MonoBehaviour
    {
        //#privates Serialized
        [SerializeField] float _rangeVision;        
        //#protected
        protected List<ObservableByVision> targetInMyArea;        
        protected ObservableByVision _currentTarget;        
        protected float _distanceTarget;
        //#
        CircleCollider2D _colliderRange;
        public ObservableByVision CurrentTarget { get => _currentTarget; }
        public float DistanceTarget { get => _distanceTarget; }


        protected virtual void Awake()
        {
            targetInMyArea = new List<ObservableByVision>();
            _colliderRange = gameObject.AddComponent<CircleCollider2D>();
            _colliderRange.radius = _rangeVision;
            _colliderRange.isTrigger = true;

        }



      
        

        public void AddObserveObject(ObservableByVision target)
        {
            targetInMyArea.Add(target);
        }
        public void RemoveObserveObject(ObservableByVision target)
        {
            targetInMyArea.Remove(target);
        }
        private void Update()
        {
            if (targetInMyArea == null) return;
            if (targetInMyArea.Count > 0)
                _currentTarget = targetInMyArea[targetInMyArea.Count - 1];
            else
                _currentTarget = null;

            if (_currentTarget != null) _distanceTarget = Vector2.Distance(transform.position,_currentTarget.transform.position);


        }

    }
}

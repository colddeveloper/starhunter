﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace StageMachineAI
{
    public class StagePatrol : StageAi
    {
        [Header("Exclusive Variables")]
        [SerializeField] Movment _movment;

        public override void EnableStage()
        {
            base.EnableStage();
            _movment.StopMovment();
        }

    }

}

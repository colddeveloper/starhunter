﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class AudioListenerCenter : MonoBehaviour
{
    [SerializeField] AudioListener _audioS;
    int _photonViewIDMyPlayer;
    private void Awake()
    {
        SignalsGame.Listen_OnMyPlayerSpawn += OnPlayerSpawn;
        SignalsGame.Listen_OnDie += OnPlayerDie;
    }
    private void OnDestroy()
    {
        SignalsGame.Listen_OnMyPlayerSpawn -= OnPlayerSpawn;
        SignalsGame.Listen_OnDie -= OnPlayerDie;
    }
    void OnPlayerSpawn(int PviewID)
    {
        _photonViewIDMyPlayer = PviewID;
        _audioS.enabled = false;
    }
    void OnPlayerDie(int viewId_assassin,int vitime)
    {
        if(_photonViewIDMyPlayer == vitime)
        _audioS.enabled = true;
    }

   
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{

    private AudioSource as_s;
    public AudioClip currentMusic_v;
    private AudioClip musicPLaying_v;
    private float speed_v = 0.9f;
    private float maxVolume_v;
    private float maxVolume_a;

    private static bool smoothVolume_v;

    public static void smoothVolume_f()
    {
        smoothVolume_v = true;
    }
    public static void defaultVolume_f()
    {
        smoothVolume_v = false;
    }

    private void Awake()
    {
        maxVolume_v = 1.0f;
        as_s = GetComponent<AudioSource>();
        musicPLaying_v = as_s.clip;
        as_s.volume = maxVolume_v;
       
        //as_s.mute = true;
    }
    private float startTime;


    void Start()
    {
        DontDestroyOnLoad(gameObject);
        maxVolume_v = as_s.volume;
        startTime = Time.unscaledTime;
        //as_s.mute = true;
    }



    /*public void setNewMusic_f(AudioClip ac)
    {
        currentMusic_v = ac;
    }*/

    public void changeMusic_f(AudioClip ac,float speed,bool loop,float time)
    {
        StartCoroutine(change_f(ac,speed,loop,time));
    }

    IEnumerator change_f(AudioClip ac, float speed, bool loop,float time)
    {
        yield return new WaitForSecondsRealtime(time);
        currentMusic_v = ac;
        startTime = Time.unscaledTime;
        musicPLaying_v = currentMusic_v;
        speed_v = speed;
        as_s.loop = loop;
    }


    private bool changing_v;

    

    private void Update()
    {
      
        maxVolume_a = maxVolume_v;
        if (smoothVolume_v) maxVolume_a *= 0.2f;


        
        if (musicPLaying_v != as_s.clip)
        {
            as_s.volume = Mathf.Lerp(maxVolume_a, 0, speed_v * (Time.unscaledTime - startTime));
            if(as_s.volume == 0){
                changing_v = true;
                as_s.clip = musicPLaying_v;
                as_s.Play();
                startTime = Time.unscaledTime;
            }
        }
        else
        {
            if(as_s.volume != maxVolume_a)
            as_s.volume = Mathf.Lerp(as_s.volume, maxVolume_a, speed_v * (Time.unscaledTime - startTime));
        }

       
    }


    private void LateUpdate()
    {
        //bool mute = true;
        //if (dataConfig.getEnableMusic_f() == 1) mute = false;
        //if(as_s.mute != mute) as_s.mute = mute;
    }




}

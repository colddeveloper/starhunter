﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddUiAudio : MonoBehaviour
{
    [SerializeField] AUDIO_UI _audio;
    [SerializeField] bool _loop;
    [SerializeField] float _secondsDestroyAudio;
    [SerializeField] bool _playOnAwake;


    private void Awake()
    {
        if (_playOnAwake)
            ActionAddAudio();
    }
    public void ActionAddAudio()
    {
       SignalsAudio.Dispatch_InstanceAudioUI(_audio, _loop, _secondsDestroyAudio);
    }

}

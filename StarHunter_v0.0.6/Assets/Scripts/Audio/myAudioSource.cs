﻿using UnityEngine;
public class myAudioSource : MonoBehaviour
{
    [SerializeField] bool _autoDestroy = false;
    public bool AutoDestroy { set => _autoDestroy = value; }
    AudioSource As;
    private void Awake()
    {
        //For 3D Audio perfect working
        //Para os audios 3D funcionarem perfeitamente
        As = GetComponent<AudioSource>();
        As.spatialBlend = 1.0f;
        As.minDistance = 5;
        As.maxDistance = 20;
        As.spread = 1.0f;
        As.rolloffMode = AudioRolloffMode.Linear;
        Vector3 ps = transform.position;
        ps.z = GameObject.FindWithTag("MainCamera").transform.position.z;
        transform.position = ps;  
    }

    public void Start()
    {
        if (_autoDestroy && !gameObject.GetComponent<SfxAutoDestroy>()) gameObject.AddComponent<SfxAutoDestroy>();
    }




}

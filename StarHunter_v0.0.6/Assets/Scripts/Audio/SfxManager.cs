﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class SfxManager : MonoBehaviour
{
    [SerializeField] AudioData _data;
    [SerializeField] PhotonView _pv;


    private void Start()
    {
        SignalsAudio.Listen_PlayAudioGameplay += OnPlayAudioGameplay;
        SignalsAudio.Listen_InstanceAudioGameplay += OnInstanceAudioGameplay;
        SignalsAudio.Listen_InstanceAudioUI += InstanceAudioUI;
    }
    private void OnDestroy()
    {
        SignalsAudio.Listen_PlayAudioGameplay -= OnPlayAudioGameplay;
        SignalsAudio.Listen_InstanceAudioGameplay -= OnInstanceAudioGameplay;
        SignalsAudio.Listen_InstanceAudioUI -= InstanceAudioUI;
    }



    //#Calls Listen
    void OnPlayAudioGameplay(AUDIO_GAMEPLAY key, int photonViewID_audiosource,bool online)
    {
        if (key == AUDIO_GAMEPLAY.NONE) return;
        if (online)
        {   
            _pv.RPC("PlayAudioGameplay_RPC", RpcTarget.All,key,photonViewID_audiosource);
            return;
        }
        PlayAudioGameplay(key, photonViewID_audiosource);
    }
    void OnInstanceAudioGameplay(AUDIO_GAMEPLAY key, Vector2 ps, bool autodestroy,bool online)
    {
        if (key == AUDIO_GAMEPLAY.NONE) return;
        //If instance only single player or not
        if (online)
        {   
            _pv.RPC("InstanceAudioGameplay_RPC", RpcTarget.All, key, ps, autodestroy);
            return;
        }
        InstanceAudioGameplay(key, ps, autodestroy);
    }



    //RPC Functions
    [PunRPC]
    void PlayAudioGameplay_RPC(AUDIO_GAMEPLAY key, int photonViewID_audiosource)
    {
        PlayAudioGameplay(key, photonViewID_audiosource);
    }
    [PunRPC]
    void InstanceAudioGameplay_RPC(AUDIO_GAMEPLAY key, Vector2 ps, bool autodestroy)
    {
        InstanceAudioGameplay(key, ps,autodestroy);
    }



    //#Utils
    void PlayAudioGameplay(AUDIO_GAMEPLAY key, int photonViewID_audiosource)
    {
        //----------------------------------------------------------------Get Audio Source Photon View
        AudioSource owner = null;
        PhotonView pvowner = PhotonView.Find(photonViewID_audiosource);
        if(pvowner != null)
        owner = pvowner.GetComponent<AudioSource>();
        //----------------------------------------------------------------
        if (key == AUDIO_GAMEPLAY.NONE) return;
        if (!owner) return;
        owner.clip = _data.GetGameplay(key);
        owner.Play();
    }

    void InstanceAudioGameplay(AUDIO_GAMEPLAY key, Vector2 ps, bool autodestroy)
    {
        if (key == AUDIO_GAMEPLAY.NONE) return;
        _data.Instance_Gameplay(key, ps, autodestroy);
    }

    void InstanceAudioUI(AUDIO_UI key, bool loop, float timeDestroy)
    {

        if (key == AUDIO_UI.NONE) return;
        _data.Instance_UI(key, loop, timeDestroy);
    }

}

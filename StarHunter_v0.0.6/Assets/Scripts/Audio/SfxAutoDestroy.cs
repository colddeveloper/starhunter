﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxAutoDestroy : MonoBehaviour
{
    [SerializeField] AudioSource _audioSource;
    private void Awake()
    {
        if (_audioSource == null && GetComponent<AudioSource>()) _audioSource = GetComponent<AudioSource>();
    }
    float T;
    private void Update()
    {
        if (_audioSource.isPlaying)
        {
            T = 0;
            return;
        }
        T += Time.deltaTime;
        if (T >= 0.3f) Destroy(gameObject);
    }
}

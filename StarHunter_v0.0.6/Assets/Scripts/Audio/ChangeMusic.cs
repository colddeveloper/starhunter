﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMusic : MonoBehaviour
{

    public AudioClip music_v;    
    public float speed_v = 0.9f;
    public float time;
    public bool loop_v = true;


    

    void OnEnable()
    {
        action_f();
    }

    public void action_f()
    {
        Music music_s = null;
        if (GameObject.FindWithTag("Music"))
            music_s = GameObject.FindWithTag("Music").GetComponent<Music>();

        if (!music_s)
        {
            GameObject m = new GameObject("music");
            m.tag = "Music";
            m.AddComponent<AudioSource>();
            music_s = m.AddComponent<Music>();
            m.GetComponent<AudioSource>().loop = true;
        }
        music_s.changeMusic_f(music_v, speed_v, loop_v, time);
    }

   
}
